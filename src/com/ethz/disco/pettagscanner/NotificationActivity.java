package com.ethz.disco.pettagscanner;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;


/**
 * This Activity shows the detail view of an animal loss notification.
 * It shows an image of a map which shows where the animal is located.
 * It also lists details like animal name, finder name, owner name, etc.
 */
public class NotificationActivity extends MainMenuActivity implements AsyncResponse {
    private StatusFunctions statusFunction = new StatusFunctions();
    private APIAlertDialogs alertBox = new APIAlertDialogs();
    static final String TAG = "rfiDOG";
    private Bundle extras;
    private String longitude;
    private String latitude;
    private String id;
    private String animalname;
    private String url;
    private boolean loaded = false;
    private String is_animal_owned;
    private String owner;
    
    // init all TextViews
    TextView tagView;
    TextView animalNameView;
    TextView finderNameView;
    TextView finderPhoneView;
    TextView finderTimeView;
    TextView commentView;
    ImageView mChart;
    
    // instantiate HttpRequest AsyncTask
    Request request = new Request(NotificationActivity.this);
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        request.cancel(true);
    }
    
    @Override
    protected void onRestart() {
        // check login status
        super.onRestart();
        statusFunction.amILoggedIn(this);
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            
            if (loaded) {
                Intent intent = new Intent(NotificationActivity.this,NotificationListActivity.class);
                // find out if current record id is found/lost id
                if (is_animal_owned.equals("yes")) {
                    // mode = lost
                    intent.putExtra("mode","lost");
                } else if (is_animal_owned.equals("no")) {
                    // mode = found
                    intent.putExtra("mode","found");
                } else {
                    // nop
                }
                startActivity(intent); 
                finish();
                return true;
            } else {
                // too early, so just default to lost animals
                startActivity(new Intent(NotificationActivity.this,NotificationListActivity.class));
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // add content from new layout
        ViewGroup inclusionViewGroup = (ViewGroup)findViewById(R.id.content);
        Log.i("rfiDOG","parent: "+inclusionViewGroup);
        LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View child = inflater.inflate( R.layout.notification /* resource id */,
            inclusionViewGroup /* parent */,
            false /*attachToRoot*/);
        Log.i("rfiDOG","child: "+child);
        inclusionViewGroup.addView(child);
        
        // initialize layout field
        tagView = (TextView) findViewById(R.id.animaltag);
        animalNameView = (TextView) findViewById(R.id.animalname);
        finderNameView = (TextView) findViewById(R.id.findername);
        // TODO: onclick link to finder profile, if "Anonymous" no click possible?
        finderTimeView = (TextView) findViewById(R.id.findtime);
        commentView = (TextView) findViewById(R.id.commentfield);
        finderPhoneView = (TextView) findViewById(R.id.finderphone);
        finderPhoneView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                statusFunction.callPhone(NotificationActivity.this,finderPhoneView.getText().toString().replaceAll("-",""));
            }
        });
        
        // get notification id from intent
        Intent intent = getIntent();
        extras = intent.getExtras();
        if (extras.getString("id") != null) {
            id = extras.getString("id");
        } else {
            id = "";
        }
        if (!id.isEmpty()) {
            // load record from api
            url = getResources().getString(R.string.api_url)+getResources().getString(R.string.notification_uri)+id+"/";
            // execute notification fetch request and set interface for callback listener
            request.delegate = this;
            request.execute(url,"GET",NotificationActivity.this);
        } else {
            // last activity did not pass data. cannot load record. go back one step.
            Log.wtf(TAG,"no record id provided from NotificationListActivity or GCM push notification.");	
            alertBox.simpleRedirectAlert(NotificationActivity.this,NotificationListActivity.class,"An error occurred","Okay",null);
        }
    }
    
    /**
     * Once API request result comes in, this method is called.
     * The data either contains a success or a failure message from the API.
     * If a success message is returned, the method sets the data into the fields and downloads the map image.
     * If a failure message is returned, the app redirects back to the notification list view, (where all the records are displayed).
     */
    public void processFinish(String result) {
        try {
            if(result != null) {
                JSONObject output = new JSONObject(result);
                if (output.has("findername")) {
                    // check if current login is for owner:
                    is_animal_owned = output.getString("is_animal_owned");
                    
                    // api response is good            		
                    tagView.setText(output.getString("animaltag"));
                    animalname = output.getString("animalname");
                    animalNameView.setText(output.getString("animalname"));
                    finderNameView.setText(output.getString("findername"));
                    finderPhoneView.setText(output.getString("finderphone"));
                    finderTimeView.setText(output.getString("time"));
                    commentView.setText(output.getString("comment"));
                    
                    loaded = true;	
                    latitude = output.getString("latitude");
                    longitude = output.getString("longitude");
                    String imageURL = getResources().getString(R.string.main_url) + output.getString("mapurl");
                    Log.i("rfiDOG","Notification activity debug image: ");
                    Log.i("rfiDOG","latitude: "+latitude);
                    Log.i("rfiDOG","longitude: "+longitude);
                    
                    mChart = (ImageView) findViewById(R.id.map);
                    new downloadImage().execute(imageURL);
                    Log.i(TAG,"image url: "+imageURL);
                } else if (output.has("status")) {
                    // api fetch record failed. display errors and stay. it's not a dealbreaker if the map is missing.
                    String errortext = output.getString("status");
                    alertBox.simpleRedirectAlert(NotificationActivity.this, NotificationListActivity.class, errortext, "Back",null);
                }
            } else {
                // null case
                String errortext = "Server error";
                alertBox.simpleRedirectAlert(NotificationActivity.this, NotificationListActivity.class, errortext, "Back",null);
            }
        } catch (JSONException e) {
            //Toast.makeText(LoginActivity.this,"interruption: "+result,Toast.LENGTH_LONG).show();
            e.printStackTrace();
            String errortext = "Server error";
            alertBox.simpleRedirectAlert(NotificationActivity.this, NotificationListActivity.class, errortext, "Back",null);
        }
    }
    
    /**
     * This backgroundtask fetches a map image from the API from a specified url.
     */
    public class downloadImage extends AsyncTask<String, Integer, Drawable> {

        @Override
        protected Drawable doInBackground(String... arg0) {
            // This is done in a background thread
            String url = arg0[0];
            Log.i(TAG,"image url: "+url);
            return dlImage(arg0[0]);
        }

        protected void onPostExecute(Drawable image) {
            if (image == null) {
                Log.e(TAG,"null image received.");
                alertBox.simpleAlert(NotificationActivity.this, "Error downloading map from server.", "Okay");
            } else {
                // place image in ImageView field
                mChart.setImageDrawable(image);
                mChart.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        // open up google maps api v3 activity with one marker
                        Intent intent = new Intent(NotificationActivity.this,MapActivity.class);
                        intent.putExtra("latitude",latitude);
                        intent.putExtra("longitude",longitude);
                        intent.putExtra("animalname",animalname);
                        intent.putExtra("id",id);
                        startActivity(intent);
                        finish();

                    }
                });
            }
        }
        
        private Drawable dlImage(String _url)
        {
            //Prepare to download image
            URL url;        
            BufferedOutputStream out;
            InputStream in;
            BufferedInputStream buf;
            
            //BufferedInputStream buf;
            try {
                url = new URL(_url);
                in = url.openStream();
                // Read the inputstream 
                buf = new BufferedInputStream(in);
                // Convert the BufferedInputStream to a Bitmap
                Bitmap bMap = BitmapFactory.decodeStream(buf);
                if (in != null) {
                    in.close();
                }
                if (buf != null) {
                    buf.close();
                }
                return new BitmapDrawable(bMap);
            } catch (Exception e) {
                Log.e("Error reading file", e.toString());
                //alertBox.simpleAlert(NotificationActivity.this, "Error downloading map from server.", "Okay");
            }
            //alertBox.simpleAlert(NotificationActivity.this, "Error downloading map from server.", "Okay");
            return null;
        }
    }
}