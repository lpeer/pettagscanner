package com.ethz.disco.pettagscanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

/**
 * This Class contains a number of custom alerts that are
 * used for error messages, redirects or to quit the application
 */
public class APIAlertDialogs {
    
    /**
     * Displays alert to user, that asks him if he wants
     * to quit the app or not.
     */
    public void quitOrNop(Activity activity) {
        final Activity act = activity;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(act);
        // Setting Dialog Title
        alertDialog.setTitle("Would you like to quit?");
        // Setting Dialog Message
        //alertDialog.setMessage("example message");
        alertDialog.setPositiveButton("Quit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.dismiss();
                Log.i("rfiDOG","quitting.");
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                act.startActivity(intent);
                act.finish();           	
            }});
        // on pressing  button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Log.i("rfiDOG","not quitting.");
            }});
        AlertDialog alert = alertDialog.create();
        // Show Alert Message
        alertDialog.show();
    }
    
    /**
     * Displays alert to user, informing him using a custom message
     * User has no choice but to click the only button upon which
     * the application quits.
     */
    public void quitApplicationDialog(Activity activity,String descriptionMessage,String buttonMessage) {
        final Activity act = activity;
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        //builder.setMessage("Device does not support GPS tracking.")
        builder.setMessage(descriptionMessage)
            .setCancelable(false)
            .setPositiveButton(buttonMessage, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //quit application
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    act.startActivity(intent);
                    act.finish();
                }
            });
        AlertDialog alert = builder.create();
        alert.show();
    }
    
    /**
     * Displays alert to user, that informs the user of a connection error.
     * User is presented with one button, upon clicking, he is redirected to the app login page.
     */
    public void serverIsUnreachable(Activity activity) {
        final Activity act = activity;
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setMessage("Could not connect to server.")
            .setCancelable(false)
            .setPositiveButton("Login again.", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent = new Intent(act,LoginViewActivity.class);                 
                    intent.putExtra("username", "");
                    intent.putExtra("password", ""); 
                    intent.putExtra("error", "");
                    act.startActivity(intent);
                    act.overridePendingTransition(0,0);
                    act.finish();
                }
            });
        AlertDialog alert = builder.create();
        alert.show();   
    }
    
    /**
     * Displays simple custom alert. User has one button with custom text.
     * Dialog closes upon button click. Nothing else happens.
     */
    public void simpleAlert(Activity activity,String descriptionMessage,String buttonMessage) {
        // shows custom alert and redirects back to same calling activity (reloads)  
        final Activity act = activity;
        Log.i("rfiDOG","nullpointer in simplealert with activity: "+act);
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
            builder.setMessage(descriptionMessage)
                .setCancelable(false)
                .setPositiveButton(buttonMessage, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) { /*nop stays in activity after clicking away*/ }
                });
            AlertDialog alert = builder.create();
            alert.show();   
    }
    
    /**
     * Displays simple custom alert. User has one button with custom text.
     * Dialog closes upon button click. App redirects to specific Activity after button click.
     */
    public void simpleRedirectAlert(Activity activity,Class newTask,String descriptionMessage,String buttonMessage,Bundle extras) {
        // shows custom alert and redirects back to same calling activity (reloads)  
        final Activity act = activity;
        final Class next = newTask;
        final Bundle ext = extras;
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setMessage(descriptionMessage)
            .setCancelable(false)
            .setPositiveButton(buttonMessage, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // go to new activity
                    Intent intent = new Intent(act,next); 
                    if (ext != null) {
                        intent.putExtras(ext);
                    }
                    act.startActivity(intent);
                    act.finish();
                }
            });
        AlertDialog alert = builder.create();
        alert.show();
    }
    
    /**
     * Displays simple custom alert. User has one button with custom text.
     * Dialog closes upon button click. App then artificially invokes the back button signal.
     */
    public void alertAndBack(Activity activity,String descriptionMessage,String buttonMessage) {
        // shows custom alert and redirects back to same calling activity (reloads)  
        final Activity act = activity;
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setMessage(descriptionMessage)
            .setCancelable(false)
            .setPositiveButton(buttonMessage, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // back button signal
                    act.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                }
            });
        AlertDialog alert = builder.create();
        alert.show();   

    }
    
    /**
     * Displays custom alert with two buttons with custom text.
     * Dialog closes upon any button click. App redirects to one of two provided Activities depending on clicked button.
     */
    public void twoFoldRedirectAlert(Activity activity,String descriptionMessage,String firstButton,Class first,String secondButton,Class second) {
        final Activity act = activity;
        final Class firstClass = first;
        final Class secondClass = second;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(act);
        // Setting Dialog Title
        alertDialog.setTitle(descriptionMessage);
        // Setting Dialog Message
        //alertDialog.setMessage("example message");
        alertDialog.setPositiveButton(firstButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.dismiss();
                Log.i("rfiDOG","dismissed");
                Intent intent = new Intent(act,firstClass);
                act.startActivity(intent);
                act.finish();
            }});
        // on pressing cancel button
        alertDialog.setNegativeButton(secondButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Log.i("rfiDOG","dismissed");
                Intent intent = new Intent(act,secondClass);
                act.startActivity(intent);
                act.finish();
            }});
        AlertDialog alert = alertDialog.create();
        // Showing Alert Message
        alertDialog.show();
    }

}
