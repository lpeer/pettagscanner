package com.ethz.disco.pettagscanner;

/**
 * Interface that defines the method processFinish() , which is inherited by any
 * Activity that implements this interface.
 * Used in Activities to get the resulting responses of API HTTP requests.
 */
public interface AsyncResponse {
    public void processFinish(String output);
}