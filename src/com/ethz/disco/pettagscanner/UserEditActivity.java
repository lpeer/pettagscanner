package com.ethz.disco.pettagscanner;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

/**
 * This Activity displays user information in editable fields and allows the user
 * to change his details, for example address, phone number or password.
 */
public class UserEditActivity extends Activity implements AsyncResponse {
    
    static final String TAG = "rfiDOG";  
    private StatusFunctions statusFunction = new StatusFunctions();
    private APIAlertDialogs alertBox = new APIAlertDialogs();
    private Bundle extras;
    private String token;
    private String url;
    
    // instantiate HttpRequest AsyncTask
    Request request = new Request(UserEditActivity.this);
    
    // init EditTexts
    EditText userNameView;
    EditText firstNameView;
    EditText lastNameView;
    EditText emailView;
    EditText phoneView;
    EditText addressOneView;
    EditText addressTwoView;
    EditText zipCodeView;
    EditText stateView;
    EditText cityView;
    EditText countryView;
    
    // init TextViews
    TextView userNameError;
    TextView firstNameError;
    TextView lastNameError;
    TextView emailError;
    TextView phoneError;
    TextView addressOneError;
    TextView addressTwoError;
    TextView zipCodeError;
    TextView stateError;
    TextView cityError;
    TextView countryError;
    
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            Intent intent = new Intent(UserEditActivity.this,UserActivity.class);
            startActivity(intent);  
            finish();       
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        request.cancel(true);
    }
    
    @Override
    protected void onRestart() {
        super.onRestart();
        statusFunction.amILoggedIn(this);
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.usereditpage);       
        Log.e(TAG,"in usereditactivity");
        
        // check login
        //statusFunction.amILoggedIn(this);
        
        // initialize layout fields
        userNameView = (EditText) findViewById(R.id.username);
        firstNameView = (EditText) findViewById(R.id.firstname);
        lastNameView = (EditText) findViewById(R.id.lastname);
        emailView = (EditText) findViewById(R.id.email);
        phoneView = (EditText) findViewById(R.id.phone);
        addressOneView = (EditText) findViewById(R.id.addressone);
        addressTwoView = (EditText) findViewById(R.id.addresstwo);
        zipCodeView = (EditText) findViewById(R.id.zipcode);
        stateView = (EditText) findViewById(R.id.state);
        cityView = (EditText) findViewById(R.id.city);
        countryView = (EditText) findViewById(R.id.country);
        
        // initialize error feedback fields
        userNameError = (TextView) findViewById(R.id.usernameerror);
        firstNameError = (TextView) findViewById(R.id.firstnameerror);
        lastNameError = (TextView) findViewById(R.id.lastnameerror);
        emailError = (TextView) findViewById(R.id.emailerror);
        phoneError = (TextView) findViewById(R.id.phoneerror);
        addressOneError = (TextView) findViewById(R.id.addressoneerror);
        addressTwoError = (TextView) findViewById(R.id.addresstwoerror);
        zipCodeError = (TextView) findViewById(R.id.zipcodeerror);
        stateError = (TextView) findViewById(R.id.stateerror);
        cityError = (TextView) findViewById(R.id.cityerror);
        countryError = (TextView) findViewById(R.id.countryerror);
        
        // get extras from UserActivity
        extras = getIntent().getExtras();        
        if (extras.getString("username") != null) {
                    	
            // fill fields was given by useractivity
            userNameView.setText(extras.getString("username"));
            firstNameView.setText(extras.getString("first_name"));
            lastNameView.setText(extras.getString("last_name"));
            emailView.setText(extras.getString("email"));
            phoneView.setText(extras.getString("phone"));
            addressOneView.setText(extras.getString("address_1"));
            addressTwoView.setText(extras.getString("address_2"));
            zipCodeView.setText(extras.getString("zipcode"));
            stateView.setText(extras.getString("state"));
            cityView.setText(extras.getString("city"));
            countryView.setText(extras.getString("country"));
            
        } else {
            // last activity did not pass any extra data. cannot update user profile fields
            Log.e(TAG,"no intent extra data provided to usereditactivity");       	
            alertBox.simpleRedirectAlert(UserEditActivity.this, UserActivity.class, "Error Ocurred", "Okay",null);
        }
        
        // initialize button to change password
        findViewById(R.id.changepassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG,"clicked change password button");            
                startActivity(new Intent(UserEditActivity.this,ChangePasswordActivity.class));
                finish();
            }
        });
        
        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {            
                
                // send JSON object to API
                JSONObject sendJSON = new JSONObject();
                try {
                    sendJSON.put("username",userNameView.getText().toString());
                    sendJSON.put("city",cityView.getText().toString());
                    sendJSON.put("first_name",firstNameView.getText().toString());
                    sendJSON.put("last_name",lastNameView.getText().toString());
                    sendJSON.put("country",countryView.getText().toString());
                    sendJSON.put("zipcode",zipCodeView.getText().toString());
                    sendJSON.put("phone",phoneView.getText().toString());
                    sendJSON.put("state",stateView.getText().toString());
                    sendJSON.put("address_1",addressOneView.getText().toString());
                    sendJSON.put("address_2",addressTwoView.getText().toString());
                    sendJSON.put("email",emailView.getText().toString());
                    
                    // JSON object to PUT is done. send it using async put request
                    url = getResources().getString(R.string.api_url) + getResources().getString(R.string.user_profile_uri);
                    request.delegate = UserEditActivity.this;
                    request.execute(url,"PUT",UserEditActivity.this,sendJSON);
                    
                } catch (JSONException e) {
                    // error parsing JSON
                    // data from edittext fields was corrupted either by user or from intent/server
                    e.printStackTrace();
                    
                    // let user choose to retry or go back to profile
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(UserEditActivity.this);
                    alertDialog.setTitle("Server Error");
                    alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {
                            Intent intent = new Intent(UserEditActivity.this,UserEditActivity.class);
                            intent.putExtras(extras);
                            startActivity(intent);
                            finish();
                        }});
                    alertDialog.setNegativeButton("Back to Profile", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(UserEditActivity.this,UserActivity.class);
                        startActivity(intent);
                        finish();
                        }});
                    alertDialog.show();
                }
            }
        });
 
    } 
    
    /**
     * Once API request result comes in, this method is called.
     * The data either contains a success or a failure message from the API.
     * If a success message is returned, the method redirects to the 
     * UserActivity which displays the newly changed user information.
     * If a failure message is returned, the app offers to 
     * retry or redirects back to the user detail page.
     */
    public void processFinish(String result) {
        
        try {
            if(result  != null) {
                
                JSONObject output = new JSONObject(result);
                //Log.i(TAG,"<JSONObject>\n"+output.toString()+"\n</JSONObject>");
                
                if (output.has("status")) {
                    // response has key status. data appears to be good.
                    
                    String status = output.getString("status");
                    if (status.equals("success")) {
                        Log.e(TAG,"status: success");
                        
                        // user changes were successfully set. go to userprofileactivity
                        //alertBox.simpleRedirectAlert(UserEditActivity.this,UserActivity.class,"Successfully changed information","Back",null);
                        startActivity(new Intent(UserEditActivity.this,UserActivity.class));
                        
                    } else if (status.equals("illegal input")){
                        // set all the error fields
                        if (output.has("username")) {
                            userNameError.setText(output.getString("username"));
                        }
                        if (output.has("city")) {
                            cityError.setText(output.getString("city"));
                        }
                        if (output.has("first_name")) {
                            firstNameError.setText(output.getString("first_name"));
                        }
                        if (output.has("last_name")) {
                            lastNameError.setText(output.getString("last_name"));
                        }
                        if (output.has("country")) {
                            countryError.setText(output.getString("username"));
                        }
                        if (output.has("zipcode")) {
                            zipCodeError.setText(output.getString("zipcode"));
                        }
                        if (output.has("phone")) {
                            phoneError.setText(output.getString("phone"));
                        }
                        if (output.has("state")) {
                            stateError.setText(output.getString("state"));
                        }
                        if (output.has("address_1")) {
                            addressOneError.setText(output.getString("address_1"));
                        }
                        if (output.has("address_2")) {
                            addressTwoError.setText(output.getString("address_2"));
                        }
                        if (output.has("email")) {
                            emailError.setText(output.getString("email"));
                        }
                        
                    } else {
                        // illegal user/no response on api put request response
                        Log.e(TAG,"Bad Request.");
                        Log.e(TAG,"server output: "+output.toString());    	    			
                        alertBox.simpleRedirectAlert(UserEditActivity.this,UserActivity.class,"Error connecting to server","Back",null);
                    }
                } else {
                    // no key status in response. 
                    Log.e(TAG,"no status key in output");
                    Log.i(TAG,"server output: "+output.toString());
                    
                    // display error message to either retry or go back to useractivity
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(UserEditActivity.this);
                    alertDialog.setTitle("Server Error");
                    alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {
                            Intent intent = new Intent(UserEditActivity.this,UserEditActivity.class);
                            intent.putExtras(extras);
                            startActivity(intent);
                            finish();
                        }});
                    alertDialog.setNegativeButton("Back to Profile", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(UserEditActivity.this,UserActivity.class);
                        startActivity(intent);
                        finish();
                        }});
                    alertDialog.show();
                }
                
            }
            else {
                //null result case
                alertBox.simpleRedirectAlert(UserEditActivity.this,UserActivity.class,"Error connecting to server","Back",null);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            alertBox.simpleRedirectAlert(UserEditActivity.this,UserActivity.class,"Error connecting to server","Back",null);
        }
    }
}