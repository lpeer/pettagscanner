package com.ethz.disco.pettagscanner;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

/**
 * This Activity allows the logged in user to change his password.
 * The view contains two text fields. Input is validated. After, 
 * it sends a POST request to the API and receives a success
 * or failure response.
 */
public class ChangePasswordActivity extends Activity implements AsyncResponse {
    // set activity vars
    static final String TAG = "rfiDOG";
    private String token;
    private String url;
    Bundle extras;
    // instantiate the HttpRequest Asynctask
    Request request = new Request(ChangePasswordActivity.this);
    private StatusFunctions statusFunction = new StatusFunctions();
    private APIAlertDialogs alerts = new APIAlertDialogs();
    
    // init layout
    EditText passwordViewOne;
    EditText passwordViewTwo;   
    TextView passworderror;
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // go to UserActivity on backclick
            Intent intent = new Intent(ChangePasswordActivity.this,UserActivity.class);
            startActivity(intent); 
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        request.cancel(true);
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changepassword);
        Log.e("rfiDOG","in changepasswordactivity");
        extras = getIntent().getExtras();
        
        token = statusFunction.fetchToken(this);
        
        // define and initialize the layout textfields
        passwordViewOne = (EditText) findViewById(R.id.password2);
        passwordViewTwo = (EditText) findViewById(R.id.password1);        
        passworderror = (TextView) findViewById(R.id.passworderror);
        
        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // validate two password fields:
                String password1 = passwordViewOne.getText().toString();
                String password2 = passwordViewTwo.getText().toString();
                if (password2.equals(password1)) {
                    if (password1.isEmpty() || password1 == null) {
                        passworderror.setText("Passwords must match and cannot be empty.");
                    } else { // success
                        JSONObject sendJSON = new JSONObject();
                        try {
                            sendJSON.put("password",password1);
                            url = getResources().getString(R.string.api_url) + getResources().getString(R.string.password_change_uri);
                            request.delegate = ChangePasswordActivity.this;
                            request.execute(url,"POST",ChangePasswordActivity.this,sendJSON);
                            
                        } catch (JSONException e) {
                            e.printStackTrace();
                            passworderror.setText("Malformed input. Please choose a new password.");
                        }
                    }
                } else {
                            // passwords are not the same
                            passworderror.setText("Passwords must match and cannot be empty.");
                }
            }
        });
    } // oncreate done
    
    /**
     * Once API request result comes in, this method is called.
     * The data contains a success or failure message from the API.
     * If the change is successful, the app redirects to the login page.
     */
    public void processFinish(String result) {    	
        try {
            if(result != null) {
                JSONObject output = new JSONObject(result);
                if (output.has("status")) {
                    String status = output.getString("status");
                    if (status.equals("success")) {    	    			
                        // user password was successfully changed. display alert and go to logout
                        AlertDialog.Builder builder = new AlertDialog.Builder(ChangePasswordActivity.this);
                        builder.setMessage("Successfully changed password. ")
                            .setCancelable(false)
                            .setPositiveButton("Login with new credentials", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // remove token from sharedprefs
                                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ChangePasswordActivity.this);
                                    Editor editor = prefs.edit();
                                    editor.remove("token");
                                    editor.commit();
                                    Intent intent = new Intent(ChangePasswordActivity.this,LoginViewActivity.class);	                
                                    intent.putExtra("username", "");
                                    intent.putExtra("password", ""); 
                                    intent.putExtra("error", "");
                                    startActivity(intent);
                                    ChangePasswordActivity.this.overridePendingTransition(0,0);
                                    finish();
                                }
                            });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else if (status.equals("illegal input")){    	    			
                        if (output.has("password")) {
                            passworderror.setText(output.getString("password"));
                        }
                    } else {
                        // no known status from server
                        Log.i(TAG,"Bad Request.");
                        Log.e("rfiDOG","server output: "+output.toString());
                        
                        // go back to loginviewactivity
                        Intent intent = new Intent(ChangePasswordActivity.this,LoginViewActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    // no key status in response. 
                    Log.e(TAG,"no status key in output");
                    Log.i(TAG,"Bad Request.");
                    Log.i(TAG,"server output: "+output.toString());
                    // go back to loginviewactivity
                    Intent intent = new Intent(ChangePasswordActivity.this,LoginViewActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
            else {
                //null result case
            }
        } catch (JSONException e) {
            //Toast.makeText(LoginActivity.this,"interruption: "+result,Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
}