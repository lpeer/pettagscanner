package com.ethz.disco.pettagscanner;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;


/**
 * This Activity is the base Activity of our app.
 * It defines the used layout and sets the onclicklisteners for the elements within the layout.
 * Other Activities inherit the layout and build their own content on top of it.
 */
public class MainMenuActivity extends Activity {
    private StatusFunctions statusFunction = new StatusFunctions();
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        // set main base layout used by all other inheriting activities of our app
        setContentView(R.layout.base_menu_layout);
        
        // onclickListeners for layout
        findViewById(R.id.logo).setOnClickListener(new View.OnClickListener() {
            @Override 
            public void onClick(View view) {
                Log.i("rfiDOG","clicked logo.nop.");
                findViewById(R.id.logo).setSelected(true);
                new Handler().postDelayed(new Runnable() {@Override
                    public void run() {
                        findViewById(R.id.logo).setSelected(false);}}, 150);  
            }
        });

        findViewById(R.id.action_logout).setOnClickListener(new View.OnClickListener() {
            @Override 
            public void onClick(View view) {
                findViewById(R.id.action_logout).setSelected(true);
                Log.i("rfiDOG","clicked logout");
                startActivity(new Intent(MainMenuActivity.this,LogOutActivity.class));
                new Handler().postDelayed(new Runnable() {@Override
                    public void run() {
                        findViewById(R.id.action_logout).setSelected(false);}}, 150);  
            }
        });
        
        findViewById(R.id.action_scan).setOnClickListener(new View.OnClickListener() {
            @Override 
            public void onClick(View view) {
                findViewById(R.id.action_scan).setSelected(true);
                Log.i("rfiDOG","clicked scan");
                startActivity(new Intent(MainMenuActivity.this, ScanViewActivity.class));
                overridePendingTransition(0, 0);
                finish();
                new Handler().postDelayed(new Runnable() {@Override
                    public void run() {
                        findViewById(R.id.action_scan).setSelected(false);
                    }
                }, 150);  
            }
        });
        
        findViewById(R.id.action_history).setOnClickListener(new View.OnClickListener() {
        @Override 
        public void onClick(View view) {
            findViewById(R.id.action_history).setSelected(true);
            Log.i("rfiDOG","clicked history");
            startActivity(new Intent(MainMenuActivity.this, NotificationListActivity.class));
            overridePendingTransition(0, 0);
            finish();
            new Handler().postDelayed(new Runnable() {@Override
                public void run() {
                    findViewById(R.id.action_history).setSelected(false);}}, 150);  
        }});
        
        findViewById(R.id.action_userpage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.action_userpage).setSelected(true);
                Log.i("rfiDOG","clicked user icon");
                startActivity(new Intent(MainMenuActivity.this, UserActivity.class));
                overridePendingTransition(0, 0);
                finish();
                new Handler().postDelayed(new Runnable() {@Override
                    public void run() {
                        findViewById(R.id.action_userpage).setSelected(false);}}, 150);  
            }
        });
    }
}