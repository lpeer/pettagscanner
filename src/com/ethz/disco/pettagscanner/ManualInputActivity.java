package com.ethz.disco.pettagscanner;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;


/**
 * This Activity provides a method for the user to enter an animal tag manually.
 * The main layout is extended to contain a textfield and button.
 * Once the user clicks the button, the  APIActivity is called 
 * which sends the tag and location to the API.
 */
public class ManualInputActivity extends MainMenuActivity {
    private StatusFunctions statusFunction = new StatusFunctions();
    
    @Override
    protected void onRestart() {
        // after exiting go to loginview, which leads here again if not logged in.
        super.onRestart();
        statusFunction.amILoggedIn(this);
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // go to HomeActivity
            Intent intent = new Intent(ManualInputActivity.this,ScanViewActivity.class);
            startActivity(intent); 
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        
        // add content from new layout
        ViewGroup inclusionViewGroup = (ViewGroup)findViewById(R.id.content);
        Log.i("rfiDOG","parent: "+inclusionViewGroup);
        LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View child = inflater.inflate( R.layout.device_not_found /* resource id */,
            inclusionViewGroup,
            false);
        Log.i("rfiDOG","child: "+child);
        inclusionViewGroup.addView(child);
        
        findViewById(R.id.manualtagbutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("rfiDOG","clicked manual tag button");
                
                // start LoginActivity
                Intent intent = new Intent(ManualInputActivity.this, LocalizationActivity.class);
                // take values from tag field and pass it to next activity
                EditText tagField = (EditText)findViewById(R.id.manualtag);
                String tag = tagField.getText().toString();
                intent.putExtra("tag", tag);
                
                //findViewById(R.id.manualtagbutton).playSoundEffect(0);
                startActivity(intent);
                overridePendingTransition(0, 0);
                finish();
            }
        });
    }
}
