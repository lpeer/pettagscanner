package com.ethz.disco.pettagscanner;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * Activity is main menu for our app. It inherits the base template, thus has a top and bottom menu 
 * with navigation buttons and it provides two big buttons where the user can choose to scan an animal
 * using the RFID scanner device or manually enter an animal tag.
 */
public class ScanViewActivity extends MainMenuActivity implements AsyncResponse{
    static final String TAG = "rfiDOG";
    private StatusFunctions statusFunction = new StatusFunctions();
    private APIAlertDialogs alertBox = new APIAlertDialogs();
    Request request = new Request(ScanViewActivity.this);
    
    @Override
    protected void onRestart() {
        // after exiting go to loginview, which leads here again if not logged in.
        super.onRestart();
        //statusFunction.amILoggedIn(this);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // cancel asynctask if activity is closed
        request.cancel(true);
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            alertBox.quitOrNop(ScanViewActivity.this);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        
        //setContentView(R.layout.scanview);
        // add content from new layout to base menu layout from superactivity
        ViewGroup inclusionViewGroup = (ViewGroup)findViewById(R.id.content);        
        LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View child = inflater.inflate( R.layout.scanview /* resource id */,
            inclusionViewGroup /* parent */,
            false /*attachToRoot*/);
        Log.i("rfiDOG","child: "+child);
        inclusionViewGroup.addView(child);
        
        // set onclick listeners for layout
        findViewById(R.id.action_manualscan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
                Log.e("rfiDOG","in homeactivity onclick view records");
                findViewById(R.id.action_manualscan).setSelected(true);
                new Handler().postDelayed(new Runnable() {@Override
                    public void run() {
                        findViewById(R.id.action_manualscan).setSelected(false);}}, 150);
                Intent intent = new Intent(ScanViewActivity.this, ManualInputActivity.class);
                //findViewById(R.id.view).playSoundEffect(0);
                startActivity(intent);
                overridePendingTransition(0, 0);
                finish();
            }
        });
        findViewById(R.id.action_devicescan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("rfiDOG","clicked scan");
                findViewById(R.id.action_devicescan).setSelected(true);
                new Handler().postDelayed(new Runnable() {@Override
                    public void run() {
                        findViewById(R.id.action_devicescan).setSelected(false);}}, 150);
                Intent intent = new Intent(ScanViewActivity.this, ScanActivity.class);
                //findViewById(R.id.view).playSoundEffect(0);
                startActivity(intent);
                overridePendingTransition(0, 0);
                finish();
            }
        });
        
        // display error message
        if (getIntent().getStringExtra("error") != null) {
        	String error = getIntent().getStringExtra("error");
            alertBox.simpleAlert(ScanViewActivity.this,error,"Okay");
        }
        if (getIntent().getStringExtra("popup") != null) {
        	String error = getIntent().getStringExtra("popup");
            alertBox.simpleAlert(ScanViewActivity.this,error,"Okay");
        }
        
        
        // check registration
        informApiOfRegistrationId();
        
    }
    
    /**
     * Once API request result comes in, this method is called.
     * The registration ID is sent to the API in onCreate, and the method 
     * logs the API response.
     */
    public void processFinish(String result) {
        Log.i(TAG,"registration id send to server result: "+result);
    }
    
    public void informApiOfRegistrationId() {
    	
        JSONObject RegidJSON = new JSONObject();
        try {
            String regid = statusFunction.getRegistrationId(ScanViewActivity.this);
            Log.i(TAG,"scanview got regid from prefs: "+regid);
            RegidJSON.put("regid", regid);
            String url = getResources().getString(R.string.api_url) + getResources().getString(R.string.testgcmregistration_uri);
            request.delegate = this;
            request.execute(url,"POST",ScanViewActivity.this,RegidJSON);
        }
        catch (Exception e) {
            Log.i(TAG,"scanviewactivity exception sending regid to api");
            e.printStackTrace();
        }

    	
    }
    
}
