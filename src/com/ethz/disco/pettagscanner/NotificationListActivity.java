package com.ethz.disco.pettagscanner;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;


/**
 * This Activity lists all the animal loss records found for the logged in user.
 * The view is a scrolling list with a map image and some further details about the retrieval.
 */
public class NotificationListActivity extends MainMenuActivity implements AsyncResponse {
    
    static final String TAG = "rfiDOG";     
    private String url;
    private StatusFunctions statusFunction = new StatusFunctions();
    private APIAlertDialogs alertBox = new APIAlertDialogs();
    Context context;
    String mode;
    
    // instantiate HttpRequest AsyncTask
    Request request = new Request(NotificationListActivity.this);
    
    // init all TextViews
    LinearLayout ll;
    TextView tagView;
    TextView animalNameView;
    TextView finderNameView;
    TextView finderPhoneView;
    TextView finderTimeView;
    ImageView mChart;
    LinearLayout.LayoutParams layoutParams;
    
    @Override
    protected void onRestart() {
        // after exiting(home button) and restarting go to loginview
        super.onRestart();
        statusFunction.amILoggedIn(NotificationListActivity.this);
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {   
            alertBox.quitOrNop(NotificationListActivity.this);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.notification_list);
        
        Log.e("rfiDOG","in notificationListActivity");
        
        // add content from new layout
        ViewGroup inclusionViewGroup = (ViewGroup)findViewById(R.id.content);
        LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View child = inflater.inflate( R.layout.notification_list /* resource id */,
            inclusionViewGroup /* parent */,
            false /*attachToRoot*/);
        Log.i("rfiDOG","child: "+child);
        inclusionViewGroup.addView(child);
        
        // get linearlayout for programmatic adding of elements
        ScrollView sv = (ScrollView)findViewById(R.id.notifications);
        ll = (LinearLayout)findViewById(R.id.linlay);
        ll.setOrientation(LinearLayout.VERTICAL);
        context = getApplicationContext();
        
        findViewById(R.id.lost).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // restart activity with new intent extra
                findViewById(R.id.lost).setSelected(true);
                findViewById(R.id.found).setSelected(false);
                Intent intent = new Intent(NotificationListActivity.this,NotificationListActivity.class);
                intent.putExtra("mode", "lost");
                startActivity(intent);
                overridePendingTransition(0, 0);
                finish();
            }
        });
        
        findViewById(R.id.found).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // restart activity with new intent extra
                findViewById(R.id.found).setSelected(true);
                findViewById(R.id.lost).setSelected(false);
                Intent intent = new Intent(NotificationListActivity.this,NotificationListActivity.class);
                intent.putExtra("mode", "found");
                startActivity(intent);
                overridePendingTransition(0, 0);
                finish();
            }
        });
        
        // get mode from intent extras
        String api_url = getResources().getString(R.string.api_url);
        String lost_url = api_url + getResources().getString(R.string.lost_uri);
        String found_url = api_url + getResources().getString(R.string.found_uri);
        Intent intent = getIntent();       
        if (intent.getStringExtra("mode") != null) {
            mode = intent.getExtras().getString("mode"); // either "lost" or "found" or null 
            Log.i("rfiDOG","mode: "+mode);
            if (mode.equals("found")) {
                findViewById(R.id.found).setSelected(true);
                findViewById(R.id.lost).setSelected(false);
                url = found_url;
            } else if (mode.equals("lost")) {
                findViewById(R.id.lost).setSelected(true);
                findViewById(R.id.found).setSelected(false);
                url = lost_url;
            } else {
                // unknown mode. default to lost records
                findViewById(R.id.lost).setSelected(true);
                findViewById(R.id.found).setSelected(false);
                url = lost_url;
            }
        } else {
            // mode is not set somehow. default to lost records
            findViewById(R.id.lost).setSelected(true);
            findViewById(R.id.found).setSelected(false);
            url = lost_url;
        }
        
        // load records
        request.delegate = this;
        Log.i(TAG,"url: "+url);
        request.execute(url,"GET",NotificationListActivity.this);
        
    }
    
    /**
     * Once API request result comes in, this method is called.
     * The data either contains a sucess or a failure message from the API.
     * If a success message is returned, the method displays all the received records from the API in a list view.
     * If a failure message is returned, the method redirects back to the home screen and displays an error message.
     */
    public void processFinish(String result) {
        try {
            if(result != null) {
                // result is response from server.
                JSONObject output = new JSONObject(result);
                //Log.i("rfiDOG","<JSONObject>\n"+output.toString()+"\n</JSONObject>");
                int length = output.length();
                //Log.i("rfiDOG","length of jsonobject: "+length);
                
                // go through each JSON subrecord in output
                for (int c=1;c<length+1;c++) {
                    try {
                        String key = Integer.toString(c);
                        final JSONObject note = new JSONObject(output.get(key).toString());
                        
                        if (note.has("status")) {
                            // errors: display error message
                            String alert = note.getString("status");
                            if (alert.equals("no data")) {
                                // add textview that states that no data was found
                                TextView v = new TextView(NotificationListActivity.this);
                                v.setText("No records to display");
                                ll.addView(v); 
                            } else {
                                // load layout textview that states that no data was found
                                TextView v = new TextView(NotificationListActivity.this);
                                v.setText("Error loading data");
                                ll.addView(v); 
                            }
                            
                        } else if (note.has("timestamp")) {
                            // data loaded fine
                            String timestamp = note.getString("timestamp");
                            Double longitude = note.getDouble("longitude");
                            Double latitude = note.getDouble("latitude");
                            String findername = note.getString("findername");
                            String ownername = note.getString("ownername");
                            String imageurl = getResources().getString(R.string.main_url) + note.getString("mapimage");                            
                            Log.i("rfiDOG","url for images: "+imageurl);
                            ImageView mapImage = new ImageView(NotificationListActivity.this);
                            // get current record id in loop from record data
                            final String id = note.getString("id");
                            Log.i("rfiDOG","note id: "+id);
                            String animalname = note.getString("animalname");
                            animalname = animalname.substring(0, 1).toUpperCase() + animalname.substring(1);                            
                            
                            // fetch image from api and apply style
                            new downloadImage().execute(imageurl,mapImage);
                            mapImage.setScaleType(ScaleType.CENTER_CROP);
                            mapImage.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                            // calculate height of entry by looking at image ratio
                            Display display = getWindowManager().getDefaultDisplay();
                            int width = display.getWidth();  // deprecated
                            float ratio = (float)mapImage.getMeasuredWidth()/(float)mapImage.getMeasuredHeight();
                            int height = 300;
                            Log.i("rfiDOG","ratio: "+ratio);
                            Log.i("rfiDOG","width: "+width);
                            Log.i("rfiDOG","height: "+height);
                            
                            // linearlayout map holds map and is 2/3 of screen width
                            LinearLayout map = new LinearLayout(NotificationListActivity.this);
                            map.setLayoutParams(new LinearLayout.LayoutParams(width,height,1f));
                            map.addView(mapImage);
                            
                            // linearlayout texts2 holds description and is 1/3 of screen width
                            LinearLayout texts2 = new LinearLayout(NotificationListActivity.this);
                            texts2.setOrientation(LinearLayout.VERTICAL);
                            texts2.setLayoutParams(new LinearLayout.LayoutParams(width,height,1f));
                            
                            // add two fields: findtime and animalname
                            TextView timefield2 = new TextView(NotificationListActivity.this);
                            TextView aname2 = new TextView(NotificationListActivity.this);
                            aname2.setText(animalname);
                            texts2.addView(aname2);
                            timefield2.setText("found "+timestamp);
                            texts2.addView(timefield2);
                            texts2.setPadding(10,0,0,0);
                            aname2.setTextColor(getResources().getColor(R.color.holobluelight));
                            timefield2.setTextColor(getResources().getColor(R.color.holobluelight));
                            
                            // linearlayout that holds both the image and text linearlayouts
                            LinearLayout lin = new LinearLayout(NotificationListActivity.this);
                            lin.setOrientation(LinearLayout.HORIZONTAL);
                            LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                            lp1.setMargins(30, 30, 30, 30);
                            lin.addView(map);
                            lin.addView(texts2);
                            lin.setBackgroundDrawable(getResources().getDrawable(R.drawable.my_rectangle));
                            lin.setLayoutParams(lp1);
                            lin.setGravity(Gravity.CENTER_VERTICAL);
                            lin.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    // go to detailview
                                    Intent intent = new Intent(NotificationListActivity.this,NotificationActivity.class);
                                    intent.putExtra("id",id);
                                    Log.i("rfiDOG","onclick animalname. id: "+id);
                                    Log.i("rfiDOG","note: "+note.toString());
                                    startActivity(intent);
                                    finish();
                                }
                            });
                            ll.addView(lin);
                        }
                    } catch (JSONException e) {
                        // Something went wrong when parsing server response JSON data
                        String errortext = "(Json exception 1) Server error";
                        String buttontext = "Back";
                        alertBox.simpleRedirectAlert(NotificationListActivity.this,ScanViewActivity.class,errortext,buttontext,null);
                        
                        Log.e("rfiDOG","in json exception 1");
                    }
                }
            }
            else {
                //null result response case
                String errortext = "(JSON exception 2) Server error";
                String buttontext = "Back";
                alertBox.simpleRedirectAlert(NotificationListActivity.this,ScanViewActivity.class,errortext,buttontext,null);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            // Something went wrong!
            Log.e(TAG, "in json exception 2"); 
            Log.e(TAG,"error: "+e.toString());
            String errortext = "Server error";
            String buttontext = "Back";
            alertBox.simpleRedirectAlert(NotificationListActivity.this,ScanViewActivity.class,errortext,buttontext,null);
        }
    }
    
    public class downloadImage extends AsyncTask<Object, Integer, Drawable> {
        ImageView temp;
        
        @Override
        protected Drawable doInBackground(Object... arg0) {
            // This is done in a background thread
            String fetchurl = (String) arg0[0];
            temp = (ImageView) arg0[1];
            Log.i(TAG,"image url: "+fetchurl);
            return dlImage(fetchurl);
        }
        
        /**
         * Called after the image has been downloaded
         * -> this calls a function on the main thread again
         */
        protected void onPostExecute(Drawable image) {
            if (image == null) {
                Log.e(TAG,"null image received.");
                alertBox.simpleAlert(NotificationListActivity.this, "Error downloading image from server.", "Okay");
            } else {
                // place image in ImageView field
                temp.setImageDrawable(image);
            }
        }
        private Drawable dlImage(String _url) {
            //Prepare to download image
            URL url;        
            BufferedOutputStream out;
            InputStream in;
            BufferedInputStream buf;
            
            try {
                url = new URL(_url);
                in = url.openStream();
                // Read the inputstream 
                buf = new BufferedInputStream(in);
                // Convert the BufferedInputStream to a Bitmap
                Bitmap bMap = BitmapFactory.decodeStream(buf);
                if (in != null) {
                    in.close();
                }
                if (buf != null) {
                    buf.close();
                }
                return new BitmapDrawable(bMap);
            } catch (Exception e) {
                Log.e("Error reading file", e.toString());
            }
            return null;
        }
    }
}