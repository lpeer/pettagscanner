package com.ethz.disco.pettagscanner;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.Window;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;


/**
 * This Activity is not used in our chain.
 * It can be invoked if we specifically choose to unregister a phone
 * with the GCM cloud server.
 */
public class UnregistrationActivity extends Activity {
    
    // variables used for Google Cloud Messaging
    static final String TAG = "rfiDOG";
    private StatusFunctions statusFunction = new StatusFunctions();
    private APIAlertDialogs alertBox = new APIAlertDialogs();
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    //public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    String SENDER_ID;
    GoogleCloudMessaging gcm;
    AtomicInteger msgID = new AtomicInteger();
    SharedPreferences prefs;
    Context context;
    String regid;
    private String token;
    private boolean foreground = true;
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // suppress back function here
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    protected void onRestart() {
        super.onRestart();
        checkPlayServices();
        // nop, since it redirects to login anyway
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        
        Log.e("rfiDOG","in unregister activity");
        
        SENDER_ID = getResources().getString(R.string.gcmsenderid);
        context = getApplicationContext();
        
        // Check device for Play Services APK.
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = statusFunction.getRegistrationId(UnregistrationActivity.this);
            Log.i(TAG,"init regid value: "+regid);
            
            // unregister
            unregisterInBackground();

        } else {
            Log.i(TAG,"No valid Google Play Services APK found.");
            // cannot find/get google play services. QUIT app.
            alertBox.quitApplicationDialog(UnregistrationActivity.this,"Device does not support Google Play Services","Quit");
        }
    }
    
    private void moveOn() {
        Intent intent = new Intent(UnregistrationActivity.this,LoginViewActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
    
    // need to do the Play Services APK check here
    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
    }
    
    
    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("rfiDOG", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
    
    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
    
    /**
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    
    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return PreferenceManager.getDefaultSharedPreferences(UnregistrationActivity.this);
    }
    
    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    private void unregisterInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    
                    // delete regid from shared prefs
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(UnregistrationActivity.this);
                    Editor editor = prefs.edit();
                    editor.remove("registration_id");
                    editor.commit();
                    regid = "";                    
                    
                    
                    //unregister from gcm
                    gcm.unregister();
                    msg = "Device unregistered now, registration ID=" + regid;
                    Log.i(TAG,"unregistered from gcm in background: "+regid);
                    
                    // send registrationid to server on a number of requests: 
                    //statusFunction.sendRegistrationIdToBackend(RegistrationActivity.this,regid);
                    
                    // store registration id in apps shared preferences - no need to register again.
                    //statusFunction.storeRegistrationId(UnregistrationActivity.this, regid,PROPERTY_REG_ID,PROPERTY_APP_VERSION);
                    
                } catch (IOException ex) {
                    // could not get registration id
                    Log.i(TAG,"exception in unregister background");
                    msg = "Error :" + ex.getMessage();
                    ex.printStackTrace();
                }
                return msg;
            }
            
            @Override
            protected void onPostExecute(String msg) {
                
                if (msg.startsWith("Error: ")) {
                    // Error getting regID
                    Log.e(TAG,"Error getting regid. quitting");
                    
                    // cannot find regID. Have user select if he wants to restart RegistrationActivity again or quit
                    // exponential backoff sort of, since user reaction is not immediate
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(UnregistrationActivity.this);
                    alertDialog.setTitle("Error unregistering device registration id from google");
                    //alertDialog.setMessage("example message");
                    alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {
                            // restart RegistrationActivity
                            Intent intent = new Intent(UnregistrationActivity.this,UnregistrationActivity.class);
                            startActivity(intent);
                            finish();
                        }});
                    // on pressing cancel button
                    alertDialog.setNegativeButton("Quit", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Quit application
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    });
                } else {
                    // successfully got registration id
                    moveOn();
                }
            }
        }.execute(null, null, null);
    }
}
