package com.ethz.disco.pettagscanner;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;


/**
 * This Activity attempts to connect to a scanner device using a serial bluetooth connection.
 * If no device is found, the Activity displays an error message and goes back to the main menu.
 * If a device is found and an animal tag was scanned, it gives the tag to the LocalizationActivity for 
 * further processing.
 */
public class ScanActivity extends MainMenuActivity {
	static volatile boolean active = false;
	final static String TAG = "rfiDOG";
	public AnimationDrawable scanAnimation;
	private StatusFunctions statusFunction = new StatusFunctions();
	private APIAlertDialogs alertBox = new APIAlertDialogs();
	ProgressDialog progDailog;
	private ScanTask scanTask;
	private ProgressBar spinner;
	// bluetooth
	BluetoothAdapter mBluetoothAdapter;
	BluetoothSocket mmSocket;
	BluetoothDevice mmDevice;
	OutputStream mmOutputStream;
	InputStream mmInputStream;
	volatile boolean stopWorker;
	boolean timeout = true;
	
	void turnOff() {
		spinner.setVisibility(View.GONE);
	}
	
	void closeBT() throws IOException {
		stopWorker = true;
		mmOutputStream.close();
		mmInputStream.close();
		mmSocket.close();
		//Toast.makeText(this, "Bluetooth connection closed." , Toast.LENGTH_LONG).show();
		Log.i(TAG,"Bluetooth connection closed.");
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			// go to home activity
			turnOff();
			timeout = false;
			Intent intent = new Intent(ScanActivity.this,ScanViewActivity.class);
			startActivity(intent);        	
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
    
	@Override
    public void onStart() {
       super.onStart();
       active = true;
    } 

    @Override
    public void onStop() {
       super.onStop();
       Log.e("rfiDOG","stopping activity");
       active = false;
    }
	
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		// cancel asynctask
		scanTask.cancel(true);
		Log.e("rfiDOG","destroying activity");
		active = false;
	}
	
	@Override
	protected void onRestart() {
		// check login status
		super.onRestart();
		//statusFunction.amILoggedIn(this);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.scanning);
		Log.e(TAG,"in scanactivity");
		
		// add content from specific activity layout
		ViewGroup inclusionViewGroup = (ViewGroup)findViewById(R.id.content);
		//View child = LayoutInflater.from(this).inflate(R.layout.scanview,null);
		LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View child = inflater.inflate( R.layout.scanning /* resource id */,
			inclusionViewGroup /* parent */,
			false /*attachToRoot*/);
		Log.i("rfiDOG","child: "+child);
		inclusionViewGroup.addView(child);
		
		
		// start spinner
		//ImageView scanAnimationView = (ImageView) findViewById(R.id.scanspinner);
		//scanAnimationView.setBackgroundResource(R.anim.progress_animation);
		//scanAnimation = (AnimationDrawable) scanAnimationView.getBackground();
		//scanAnimation.start();
		spinner = (ProgressBar)findViewById(R.id.scanspinner);
		spinner.setVisibility(View.VISIBLE);
		
		// start timer for scan timeout
		new Handler().postDelayed(new Runnable() {
		@Override
		public void run() {
				// This code will be executed once the timer is over
				if (timeout) {
					
					// close bluetooth after timeout
					try {
						closeBT();
					} catch (Exception e) {
						turnOff();
						Log.i(TAG,"timeout. going back to scanviewactivity with error could not close bt.");
						Log.i(TAG,"active is: "+active);
						
						
						
						if (active) {
							Intent intent = new Intent(ScanActivity.this,LoginViewActivity.class);
		                    intent.putExtra("popup","Could not close Bluetooth connection.");
		                    startActivity(intent);
		                    finish();
							//alertBox.simpleRedirectAlert(ScanActivity.this,ScanViewActivity.class,"Could not close Bluetooth connection","Back",null);
						} else {
							//post alert in next activity
						}
						
						finish();
					}	
					
					turnOff();
					Log.i(TAG,"timeout. going back to scanviewactivity with error could not connect.");
					Log.i(TAG,"active is: "+active);
					if (active) {
						Intent i = new Intent(ScanActivity.this,ScanViewActivity.class);
	                    i.putExtra("popup","Could not connect to device. Try again.");
	                    startActivity(i);
	                    finish();
						//alertBox.simpleRedirectAlert(ScanActivity.this, ScanViewActivity.class, "Could not connect to Device. Try again.","Okay",null);
					} else {
						// post alert in next activity
					}
					finish();
				}
			}
		}, 30000);
		
		// async task that handles bluetooth device interaction
		scanTask = new ScanTask();
		scanTask.execute();

    }
	
	
	/**
	 * Asynchronous background task that attempts to open a bluetooth connection
	 * to a RFID scanner device.
	 */
	public class ScanTask extends AsyncTask<Object, Integer, String> {
		
		// buffered reader thread
		Thread workerThread;
		byte[] readBuffer;
		int readBufferPosition;
		int counter;
		
		protected void onPreExecute() {
			// init
		}
		
		@Override
		protected String doInBackground(Object... arg0) {
			if (isCancelled()) {return "";}
			
			return "";
			
		}
		
		protected void onPostExecute(String result) {
			
			// open connection
			try {
				// find device (initial pairing necessary)
				findBT();
			    openBT();
			} catch (Exception e) {
				// stop timer. prevent timer handler to redirect to nochipactivity if there is a bluetooth error (maybe not persistent error, maybe no device)
				timeout = false;
				Log.e(TAG,"find/open bt exception");
				Log.e(TAG,e.getMessage());
				if (!isCancelled()) {
					startActivity(new Intent(ScanActivity.this,ScanViewActivity.class).putExtra("error","Could not connect to device 1"));
					finish();
				} else {
					// user pressed cancel. nop.
				}
				Log.i(TAG,"fail1");
			}
			
			// send signal to arduino: 'S'
			try {
				sendData();
			} catch (Exception e) {
				timeout = false;
				//alertBox.simpleRedirectAlert(ScanActivity.this, ScanViewActivity.class, "Could not connect to Device. Try again.","Okay",null);
				//finish();
				if (!isCancelled()) {
					startActivity(new Intent(ScanActivity.this,ScanViewActivity.class).putExtra("error","Could not connect to device 2"));
					finish();
				} else {
					// user pressed cancel. nop.
				}
				Log.i(TAG,"fail2");
			}
			
			
		}
		
		/**
		 * Method checks if phone has bluetooth adapter. If not it redirects to main menu with an error message.
		 * If the phone has an adapter that is turned off, it redirects to the System Settings 
		 * where the user can enable the adapter and start again.
		 */
		void findBT() {
			mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			if (mBluetoothAdapter == null) {
				timeout = false;
				if (!isCancelled()) {
					startActivity(new Intent(ScanActivity.this,ScanViewActivity.class).putExtra("error","No Bluetooth adapter found. Enter tag manually"));
					finish();
				} else {
					// user pressed cancel. nop.
				}
			}
			
			if (!mBluetoothAdapter.isEnabled()) {
				timeout = false;
				if (!isCancelled()) {
					Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
					startActivityForResult(enableBluetooth, 0);
				} else {
					// back clicked and gone back. adapter is disabled, but user wants to go back so don't do anything
				}
			}
			
			Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
			if(pairedDevices.size() > 0) {
				for(BluetoothDevice device : pairedDevices) {
					if(device.getName().equals("ArduinoMegaSlave")) { 
						mmDevice = device;
						break;
					}
				}
			}
		}
		
		/**
		 * Method opens a serial bluetooth connection socket and begins listening for data from paired and connected RFID reader device.
		 */
		void openBT() throws IOException {
			UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"); //Standard //SerialPortService ID
			mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);    
			mmSocket.connect();
			mmOutputStream = mmSocket.getOutputStream();
			mmInputStream = mmSocket.getInputStream();
			beginListenForData();
			Log.i(TAG,"Opened bluetooth connection.");
		}
		
		/**
		 * Method is called on incoming input from the RFID reader device over the bluetooth serial line.
		 * If the input is a valid animal tag, push the value to the LocalizationActivity.
		 */
		void testInput(String data) {
			
			if (data.startsWith("tag: ")) {
				// found tag sent by arduino
				
				// stop timer
				timeout = false;
				
				// remove carriage return from arduino output
				String[] tagParts = data.replace("\n","").replace("\r","").split(" ");
				String tag = tagParts[1];
				
				// close bluetooth
				try {
					
					Log.i(TAG,"buffer contents at end: "+mmInputStream.available());
					
					closeBT();
					if (!isCancelled()) {
						// not cancelled. proceed normally
						Intent intent = new Intent(ScanActivity.this, LocalizationActivity.class);
						intent.putExtra("tag", tag);        	
						startActivity(intent);
						overridePendingTransition(0, 0);
						finish();
					} else {
						// user just pressed cancel. nop.
					}
				} catch (Exception e) {
					// error closing connection
					Log.e(TAG,e.getMessage());
					
					if (!isCancelled()) {
						startActivity(new Intent(ScanActivity.this,ScanViewActivity.class).putExtra("error","Could not close Bluetooth connection. Try again."));
						finish();
					} else {
						// user pressed cancel. nop.
					}
				}
			}
		}
		
		/**
		 * begins listening for data on serial bluetooth line connected to RFID reader device. Test data on every incoming data chunk.
		 */
		void beginListenForData() {
			
			final Handler handler = new Handler();
			final byte delimiter = 10;
			try {
				Log.i(TAG,"buffer contents: "+mmInputStream.available());
			} catch (Exception ex) {
				Log.i(TAG,"cannot read buffer contents");
				ex.printStackTrace();
			}
			stopWorker = false;
			readBufferPosition = 0;
			try {
				readBufferPosition = mmInputStream.available();
			} catch (Exception ex) {
				Log.i(TAG,"cannot set readBufferPosition to last");
			}
			
			readBuffer = new byte[1024];
			workerThread = new Thread(new Runnable() {
				public void run() {
					while(!Thread.currentThread().isInterrupted() && !stopWorker) {
						try {
							int bytesAvailable = mmInputStream.available();            
							if(bytesAvailable > 0) {
								byte[] packetBytes = new byte[bytesAvailable];
								mmInputStream.read(packetBytes);
								for(int i=0;i<bytesAvailable;i++) {
									byte b = packetBytes[i];
									if(b == delimiter) {
										byte[] encodedBytes = new byte[readBufferPosition];
										System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
										final String data = new String(encodedBytes, "US-ASCII");
										readBufferPosition = 0;
										//call testInput function every time arduino data is read
										handler.post(new Runnable() { public void run() {testInput(data);} }); 
									} else {
										readBuffer[readBufferPosition++] = b;
									}
								}
							}
						} catch (Exception ex) {
							Log.e(TAG,"exception in workerthread");
							Log.e(TAG,ex.getMessage());
							stopWorker = true;
						}
					}
				}
			});
			workerThread.start();
		}
		
		/**
		 * sends signal to RFID reader device to initialize animal scanning mode.
		 */
		void sendData() throws IOException {
			
			mmOutputStream.write('$');
			Log.i(TAG,"Sent '$' command to arduino.");
		}
	}
}

