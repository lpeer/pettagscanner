package com.ethz.disco.pettagscanner;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.TextView;

/**
 * This Activity is called when a user tries to log using his credentials.
 */
public class LoginActivity extends Activity implements AsyncResponse {
    static final String TAG = "rfiDOG";
    Request request = new Request(LoginActivity.this);
    private StatusFunctions statusFunction = new StatusFunctions();
    private APIAlertDialogs alertBox = new APIAlertDialogs();
    private String initialusername;
    private String url;
    private String username = null;
    private String password = null;
    private String referer = null;
    private TextView mTextView;
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // // go to loginviewactivity
            // Intent intent = new Intent(LoginActivity.this,LoginViewActivity.class);
            // startActivity(intent);
            // finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);		
        setContentView(R.layout.login);
        Log.e("rfiDOG","in loginactivity");
        
        // initialize login error field
        mTextView = (TextView) findViewById(R.id.loginerrorsfield);
        mTextView.setText("");
        
        Intent intent = getIntent();
        if (intent.getStringExtra("username") != null) {
            username = intent.getStringExtra("username");
        } else {
            username = "";
        }
        if (intent.getStringExtra("password") != null) {
            password = intent.getStringExtra("password");
        } else {
            password = "";
        } 
        // is referer extra set?    
        if (intent.getStringExtra("referer") != null) {
            referer = intent.getExtras().getString("referer");  
        } else {
            referer = "";
        }
        if (intent.getStringExtra("initialusername") != null) {
            initialusername = intent.getExtras().getString("initialusername");
        } else {
            initialusername = "";
        }
        
        JSONObject LoginJSON = new JSONObject();
        try {
            LoginJSON.put("username", username);
            LoginJSON.put("password", password);
            
            url = getResources().getString(R.string.api_url) + getResources().getString(R.string.login_uri);
            request.delegate = this;
            request.execute(url,"POST",LoginActivity.this,LoginJSON);
            
        } catch (JSONException e) {
            e.printStackTrace();
            Bundle ex = null;
            if (!referer.equals("")) {
                ex = new Bundle();
                ex.putString("referer",referer);
                if (!initialusername.equals("")) {
                    ex.putString("initialusername",initialusername);
                }
            }
            Log.i("rfiDOG","json1 login error");
            alertBox.simpleRedirectAlert(this,LoginViewActivity.class,"Login error","Try again",ex);
        }
        
    }// done oncreate
    
    
    /**
     * Once API request result comes in, this method is called.
     * The data either contains a token or a failure message from the API.
     * If a token is returned, the method stores the value and redirects to the main app view.
     * If a failure message is returned, the app redirects to the login view page with an error dialog.
     */
    public void processFinish(String result) {
        Log.e("rfiDOG","in loginactivity processFinish");
        try {
            if(result != null) {
                JSONObject output = new JSONObject(result);
                
                // does "token" key exist in webapp response?
                if (output.has("token")) {
                    // login successful
                    String token = output.getString("token");
                    
                    // store token in session data (privately)
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
                    Editor editor = prefs.edit();
                    editor.putString("token",token);
                    editor.commit();
                    
                    if (referer.equals("")) {
                        Intent intent = new Intent(LoginActivity.this,ScanViewActivity.class);	                
                        startActivity(intent);
                        LoginActivity.this.overridePendingTransition(0,0);
                        finish();
                    } else {
                        // referer was set, move on to show LossNotification id=referer
                        Intent intent = new Intent(LoginActivity.this,NotificationActivity.class);
                        intent.putExtra("id",referer);
                        Log.i("rfiDOG","refer to notificationactivity with id: "+referer);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    // login failed. handle error messages
                    String errortext = "";
                    if (output.has("non_field_errors")) {
                        errortext = "Login failed with the provided credentials. Try again.";
                    }
                    if (output.has("password") || output.has("username")) {
                        // field errors
                        errortext = "Please provide both username and password values.";
                    }
                    mTextView.setText(errortext);
                    Intent intent = new Intent(LoginActivity.this,LoginViewActivity.class);	                
                    intent.putExtra("error", errortext);
                    if (!referer.equals("")) {
                        intent.putExtra("referer",referer);
                    }
                    intent.putExtra("username", username);
                    if (!initialusername.equals("")) {
                        intent.putExtra("initialusername",initialusername);
                    }
                    intent.putExtra("password", password);
                    startActivity(intent);
                    LoginActivity.this.overridePendingTransition(0,0);
                    finish();
                }
            }
            else {
                //null result case
                Log.e(TAG,"null response in loginactivity");
                Bundle ex = null;
                if (!referer.equals("")) {
                    ex = new Bundle();
                    ex.putString("referer",referer);
                    if (!initialusername.equals("")) {
                        ex.putString("initialusername",initialusername);
                    }
                }
                Log.i("rfiDOG","Null login error");
                alertBox.simpleRedirectAlert(this,LoginViewActivity.class,"Login error","Try again",ex);
            }
        } catch (JSONException e) {
            Log.e(TAG,"malformed json in server response of loginactivity.");
            e.printStackTrace();
            Bundle ex = null;
            if (!referer.equals("")) {
                ex = new Bundle();
                ex.putString("referer",referer);
                if (!initialusername.equals("")) {
                    ex.putString("initialusername",initialusername);
                }
            }
            Log.i("rfiDOG","json2 login error");
            alertBox.simpleRedirectAlert(this,LoginViewActivity.class,"Login error","Try again",ex);
        }
    }
}