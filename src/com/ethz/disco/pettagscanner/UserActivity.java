package com.ethz.disco.pettagscanner;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;


/**
 * This Activity extends the MainMenuActivity. It inherits the main menu. 
 * It displays user information and displays a button that redirects to the UserEdit Activity
 */
public class UserActivity extends MainMenuActivity implements AsyncResponse {
    
    static final String TAG = "rfiDOG";
    private StatusFunctions statusFunction = new StatusFunctions();
    private APIAlertDialogs alertBox = new APIAlertDialogs();
    private String url;
    private boolean data = false;
    
    // instantiate HttpRequest AsyncTask
    Request request = new Request(UserActivity.this);
    
    // init Layout
    TextView userNameView;
    TextView firstNameView;
    TextView lastNameView;
    TextView emailView;
    TextView phoneView;
    TextView addressOneView;
    TextView addressTwoView;
    TextView zipCodeView;
    TextView stateView;
    TextView cityView;
    TextView countryView;
    
    @Override
    protected void onRestart() {
        super.onRestart();
        statusFunction.amILoggedIn(this);
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {   
            alertBox.quitOrNop(UserActivity.this);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        // inherits main menu layout from MainMenuActivity
        //setContentView(R.layout.userpage);
        Log.e("rfiDOG","in userActivity");
        
        // add content from new layout
        ViewGroup inclusionViewGroup = (ViewGroup)findViewById(R.id.content);
        View child = LayoutInflater.from(this).inflate(R.layout.userpage_snippet,null);
        inclusionViewGroup.addView(child);
        
        
        // initialize layout fields
        userNameView = (TextView) findViewById(R.id.username);
        firstNameView = (TextView) findViewById(R.id.firstname);
        lastNameView = (TextView) findViewById(R.id.lastname);
        emailView = (TextView) findViewById(R.id.email);
        addressOneView = (TextView) findViewById(R.id.addressone);
        addressTwoView = (TextView) findViewById(R.id.addresstwo);
        zipCodeView = (TextView) findViewById(R.id.zipcode);
        stateView = (TextView) findViewById(R.id.state);
        cityView = (TextView) findViewById(R.id.city);
        countryView = (TextView) findViewById(R.id.country);
        phoneView = (TextView) findViewById(R.id.phone);
        phoneView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // call phone number onclick
                String phone_no = phoneView.getText().toString().replaceAll("-","");
                statusFunction.callPhone(UserActivity.this,phone_no);
            }
        });
        
        
        findViewById(R.id.edituserbutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
                Log.e("rfiDOG","clicked edit user button");
                
                if (data) {
                    Log.i(TAG,"data is ready. proceeding");
                    // start EditUserActivity only when data profile data has come in from api
                    Intent intent = new Intent(UserActivity.this, UserEditActivity.class);
                    // take values from fields and pass them as extras to next activity
                    intent.putExtra("username", userNameView.getText().toString());
                    intent.putExtra("last_name",lastNameView.getText().toString());
                    intent.putExtra("first_name",firstNameView.getText().toString());
                    intent.putExtra("email",emailView.getText().toString());
                    intent.putExtra("phone",phoneView.getText().toString());
                    intent.putExtra("address_1",addressOneView.getText().toString());
                    intent.putExtra("address_2",addressTwoView.getText().toString());
                    intent.putExtra("zipcode",zipCodeView.getText().toString());
                    intent.putExtra("city",cityView.getText().toString());
                    intent.putExtra("state",stateView.getText().toString());
                    intent.putExtra("country",countryView.getText().toString());
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    finish();
                } else {
                    Log.i(TAG,"data is not ready yet. edit button click did nothing.");
                }
            }
        });
        
        // retrieve user information from server
        url = getResources().getString(R.string.api_url) + getResources().getString(R.string.user_profile_uri);        
        request.delegate = this;
        request.execute(url,"GET",UserActivity.this);
        Log.i(TAG,"getting user info from server: "+url);
    }
    
    /**
     * Once API request result comes in, this method is called.
     * The data either contains a success or a failure message from the API.
     * If a success message is returned, the method populates the user information fields in the layout.
     * If a failure message is returned, the app offers to retry or redirects back to the home menu.
     */
    public void processFinish(String result) {
        Log.i(TAG,"receiving result from server: "+result);
        try {
            if(result != null) {
                // result is response from server.
                
                JSONObject output = new JSONObject(result);
                Log.i(TAG,"server response on user profile fetch: " + output.toString());
                
                if (output.has("username")) {
                    // response has key username. data appears to be good.
                    
                    Log.i(TAG,"server response is good. setting values into fields");
                    
                    // fill data into fields of layout
                    userNameView.setText(output.getString("username"));
                    lastNameView.setText(output.getString("last_name"));
                    firstNameView.setText(output.getString("first_name"));
                    emailView.setText(output.getString("email"));
                    Log.e(TAG,"email in useractivity getfromresponse: "+output.getString("email"));
                    Log.e(TAG,"email in useractivity getfromresponse textview: "+emailView.getText().toString());
                    phoneView.setText(output.getString("phone"));
                    addressOneView.setText(output.getString("address_1"));
                    addressTwoView.setText(output.getString("address_2"));
                    zipCodeView.setText(output.getString("zipcode"));
                    cityView.setText(output.getString("city"));
                    stateView.setText(output.getString("state"));
                    countryView.setText(output.getString("country"));
                    
                    // text is ready to be edited (if user clicks edit button now)
                    data = true;
                    
                } else {
                    //output has no username key
                    Log.e(TAG,"output has no key username.");
                    Log.e(TAG,"output: "+output.toString());
                    
                    // display error message to either retry or go back home.
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(UserActivity.this);
                    alertDialog.setTitle("Server Error KeyError");
                    alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {
                            Intent intent = new Intent(UserActivity.this,UserActivity.class);
                            startActivity(intent);
                            finish();
                        }});
                    alertDialog.setNegativeButton("Back", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(UserActivity.this,ScanViewActivity.class);
                        startActivity(intent);
                        finish();
                        }});
                    alertDialog.show();
                    
                }
            }
            else {
                //null result case
                Log.i(TAG,"null response case in fetch user info from api.");
                
                // display an error message to either retry or go back home.
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(UserActivity.this);
                alertDialog.setTitle("Server Error Null");
                alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        Intent intent = new Intent(UserActivity.this,UserActivity.class);
                        startActivity(intent);
                        finish();
                    }});
                alertDialog.setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(UserActivity.this,ScanViewActivity.class);
                    startActivity(intent);
                    finish();
                    }});
                alertDialog.show();
                
            }
        } catch (JSONException e) {
            Log.e(TAG,"JSON parsing error");
            e.printStackTrace();
            
            // display an error message to either retry or go back home
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(UserActivity.this);
            alertDialog.setTitle("Server Error JSONException");
            alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int which) {
                    Intent intent = new Intent(UserActivity.this,UserActivity.class);
                    startActivity(intent);
                    finish();
                }});
            alertDialog.setNegativeButton("Back", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(UserActivity.this,ScanViewActivity.class);
                startActivity(intent);
                finish();
                }});
            alertDialog.show();
        }
    }
}