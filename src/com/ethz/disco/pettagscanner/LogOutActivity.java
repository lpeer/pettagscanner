package com.ethz.disco.pettagscanner;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.TextView;

/**
 * This Activity is called when a user logs out of the application.
 * It sends a logout signal to the API which refreshes the user authentication token.
 * Additionally, the Activity destroys the token from the application database.
 */
public class LogOutActivity extends Activity implements AsyncResponse {
    static final String TAG = "rfiDOG";
    Request request = new Request(LogOutActivity.this);
    private StatusFunctions statusFunction = new StatusFunctions();
    private APIAlertDialogs alertBox = new APIAlertDialogs();
    private String url;
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);		
        setContentView(R.layout.login);
        Log.e("rfiDOG","in logoutactivity");
        // call url
        url = getResources().getString(R.string.api_url) + getResources().getString(R.string.logout_uri);
        // execute notification fetch request and set interface for callback listener
        request.delegate = this;
        request.execute(url,"GET",LogOutActivity.this);
    } // done oncreate
    
    /**
     * Once API request result comes in, this method is called.
     * The data either contains a success or a failure message from the API.
     * If a success message is returned, the method deletes the API authentication token from the local database.
     * If a failure message is returned, the method proceeds as above but logs the error.
     */
    public void processFinish(String result) {
        Log.e("rfiDOG","in logoutactivity processFinish");
        try {
            if(result != null) {
                JSONObject output = new JSONObject(result);
                // does "token" key exist in webapp response?
                if (output.has("success")) {
                    // logout successful
                    statusFunction.logOut(LogOutActivity.this);
                } else {
                    // logout failed. still logout of app, but log error
                    Log.e(TAG,"Logout failed. No key 'success'.");
                    statusFunction.logOut(LogOutActivity.this);
                }
            }
            else {
                //null result case
                Log.e(TAG,"null response in logoutactivity");
                statusFunction.logOut(LogOutActivity.this);
            }
        } catch (JSONException e) {
            Log.e(TAG,"malformed json in server response of logoutactivity.");
            e.printStackTrace();
            statusFunction.logOut(LogOutActivity.this);
        }
    }
}