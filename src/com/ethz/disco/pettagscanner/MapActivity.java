package com.ethz.disco.pettagscanner;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * This Activity displays a Google Map which displays 
 * a marker for where the animal is located.
 */
public class MapActivity extends FragmentActivity {
    /**
     * Note that this may be null if the Google Play services APK is not available.
     */
    private GoogleMap mMap;
    private double latitude;
    private double longitude;
    private String animalname;
    private boolean showMarker = true;
    private APIAlertDialogs alertBox = new APIAlertDialogs();
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (getIntent().getStringExtra("id") != null) {
                String id = getIntent().getStringExtra("id");
                startActivity(new Intent(MapActivity.this,NotificationActivity.class).putExtra("id", id));
                finish();
            } else {
                startActivity(new Intent(MapActivity.this,NotificationListActivity.class));
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basic_demo);
        
        
        
        // get lan lon from extras
        Bundle extras = getIntent().getExtras();
        try {
            latitude = Double.parseDouble(extras.getString("latitude"));
            longitude = Double.parseDouble(extras.getString("longitude"));
            animalname = extras.getString("animalname");
        } catch (Exception e) {
            Log.e("rfiDOG","Error providing latlon to map.");
            alertBox.simpleAlert(MapActivity.this,"Error getting Location.","Okay");
            e.printStackTrace();
            showMarker = false;
        }
        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
                mMap.setMyLocationEnabled(true);
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera.
     */
    private void setUpMap() {
        if (showMarker) {
            mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title(animalname));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude),15));
        }
        
    }
}
