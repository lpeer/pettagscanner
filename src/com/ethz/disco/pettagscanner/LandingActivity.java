package com.ethz.disco.pettagscanner;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

/**
 * This Activity is called when a new notification comes in.
 * It makes sure that the right user is logged in to see the
 * notification detail page. If no user is logged in, it redirects to
 * the login page with a special message, informing the user in question to log in.
 */
public class LandingActivity extends MainMenuActivity implements AsyncResponse {

    StatusFunctions statusFunction = new StatusFunctions();
    APIAlertDialogs alertBox = new APIAlertDialogs();
    private String referer; 
    SharedPreferences prefs;
    Context context;
    String token;
    String username;
    String id;
    String animalname;
    static final String TAG = "rfiDOG";
    
    // instantiate HttpRequest AsyncTask
    Request request = new Request(LandingActivity.this);
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            alertBox.quitOrNop(LandingActivity.this);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        request.cancel(true);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        
        Log.e("rfiDOG", "in landing activity");
        context = getApplicationContext();
        
        // get values from context and preferences
        Intent i = getIntent();
        if (i.getStringExtra("username") != null) {
        	username = i.getStringExtra("username");
        } else {
            username = "";
            // error because username needs to be set. it comes from GCM, which takes data from API
            Log.wtf(TAG,"new notification links to empty user.");
            startActivity(new Intent(LandingActivity.this,LoginViewActivity.class).putExtra("popup","Malformed GCM Push Notification."));
            finish();
        }
        if (i.getStringExtra("id") != null) {
            id = i.getStringExtra("id");
        } else {
            // error because id needs to be set. it comes from GCM, which takes data from API
            id = "";
            Log.wtf(TAG,"new notification links to no id.");
            startActivity(new Intent(LandingActivity.this,LoginViewActivity.class).putExtra("popup","Malformed GCM Push Notification."));
            finish();
        }
        if (i.getStringExtra("animalname") != null) {
            animalname = i.getStringExtra("animalname");
        } else {
            animalname = "";
            Log.wtf(TAG,"new notification links to no finder.");
            startActivity(new Intent(LandingActivity.this,LoginViewActivity.class).putExtra("popup","Malformed GCM Push Notification."));
            finish();
        }
        
        token = statusFunction.fetchToken(this);
        if (token.isEmpty()) {
            // go to login with username extra set
            Intent intent = new Intent(LandingActivity.this,LoginViewActivity.class);
            intent.putExtra("referer",id);
            intent.putExtra("username", username);
            intent.putExtra("initialusername", username);
            intent.putExtra("password","");
            intent.putExtra("error","Hi, "+username+" enter your password to see where "+animalname+" has been found.");
            startActivity(intent);
            finish();
        } else {
            // token exists in shared preferences. does it belong to the user that owns the notification animal?
            String url = getResources().getString(R.string.api_url) + getResources().getString(R.string.user_validate_uri);
            request.delegate = this;
            request.execute(url,"GET",this);
        }
        
    }
    
    /**
     * Once API request result comes in, this method is called.
     * The data contains the username of the logged in user, or an unauthorized message
     * if no user is logged in. If a username comes back, the method redirects to the
     * notification page, if not, it redirects to the login view.
     */
    public void processFinish(String response) {
        try {
            Log.i(TAG,"server response: "+response);
            JSONObject output = new JSONObject(response);
            String status = output.getString("detail");
            if (status.equals("success")) {
                // token is valid. check if extras username is equal to status username
                String usr = output.getString("username");
                if (usr.equals(username)) {
                    // redirect to notificationactivity
                    startActivity(new Intent(LandingActivity.this,NotificationActivity.class).putExtra("id",id));
                    finish();
                } else {
                    // force logout so the right user sees notification
                    // remove token from sharedprefs
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LandingActivity.this);
                    Editor editor = prefs.edit();
                    editor.remove("token");
                    editor.commit();
                    // go to login with notification username and id set as extras
                    Intent intent = new Intent(LandingActivity.this,LoginViewActivity.class);
                    intent.putExtra("referer",id);
                    intent.putExtra("username", username);
                    intent.putExtra("initialusername",username);
                    intent.putExtra("password","");
                    intent.putExtra("error","Hi, "+username+"! Log in to see where "+animalname+" has been found.");
                    startActivity(intent);
                    finish();
                }
            } else if (status.equals("Invalid token")) {
                // not logged in
                // token is illegal/outdated. delete it from shared preferences and go to login
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LandingActivity.this);
                Editor editor = prefs.edit();
                editor.remove("token");
                editor.commit();
                // redirect to login
                Intent intent = new Intent(LandingActivity.this,LoginViewActivity.class);
                intent.putExtra("referer",id);
                intent.putExtra("username", username);
                intent.putExtra("initialusername", username);
                intent.putExtra("password","");
                intent.putExtra("error","Hi, "+username+" enter your password to see where "+animalname+" has been found.");
                startActivity(intent);
                finish();
                
            } else {
                // unknown status. API error
                startActivity(new Intent(LandingActivity.this,LoginViewActivity.class).putExtra("popup","API Error."));
                finish();
            }
        } catch (JSONException e) {
            // json error in server response
            startActivity(new Intent(LandingActivity.this,LoginViewActivity.class).putExtra("popup","An error occurred."));
            finish();
        }
    }
}
