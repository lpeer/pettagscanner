package com.ethz.disco.pettagscanner;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;

/**
 * This Activity shows a logo for a set amount of time and then moves
 * on to the next Activity in the chain.
 */
public class SplashScreen extends Activity {
 
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    private StatusFunctions statusFunction = new StatusFunctions();
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        
        Log.e("rfiDOG","in splashscreen");
                
        // set timer for SplashScreen
        new Handler().postDelayed(new Runnable() {
        
            @Override
            public void run() {
                // This method will be executed once the timer is over
                //Intent i = new Intent(SplashScreen.this, UnregistrationActivity.class);
                Intent i = new Intent(SplashScreen.this, RegistrationActivity.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);              
    }
}
