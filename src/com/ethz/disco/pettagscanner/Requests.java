package com.ethz.disco.pettagscanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;


/**
 * The Request class provides our way of communicating with the API.
 * It takes a url, an HTTP method descriptor, an activity and a possible JSON object as parameters.
 * Based on the input, it then communicates with the API using either a GET,POST or PUSH method to the desired url.
 * The API response is returned to the invoking Activity through the use of an interface class called AsyncResponse.
 */
class Request extends AsyncTask<Object,Void,String>{
    private String TAG = "rfiDOG";
    public AsyncResponse delegate = null;
    public Activity activity = null;
    private StatusFunctions statusFunctions = new StatusFunctions();
    private APIAlertDialogs alertBox = new APIAlertDialogs();
    private String token;
    private ProgressDialog progDialog;
    
    public Request(Activity act) {
         super();
         activity = act;
    }
    
    @Override
    protected void onPreExecute() {
        // display progressdialog
        progDialog = new ProgressDialog(activity);
        progDialog.setMessage("Loading...");
        progDialog.setIndeterminate(false);
        //progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setCancelable(true);
        progDialog.show();
        progDialog.setContentView(R.layout.dialog); 
        
   }
   
    @Override
    protected String doInBackground(Object... params) {
        activity = (Activity)params[2];
        
        // fetch parameters and set return value of asynctask
        String url = (String)params[0];
        String method = (String)params[1];
        
        JSONObject sendData = null;
        String resultString = null;      
        token = statusFunctions.fetchToken(activity);
        // set POST/PUT object for GET it's null but unused
        if (!method.equals("GET")) {
           sendData = (JSONObject)params[3];
        }
        
        // prepare/perform http request
        try {
        
            HttpEntity entity = null; 
            HttpResponse response = null;	
            int timeoutConnection = 25000;
            int timeoutSocket = 30000;
            String regID = statusFunctions.getRegistrationId(activity);
            Log.i(TAG,"registration id in requests.java: "+regID);
            if (regID.equals("")) {
                // no registration id found
                // TODO: redirect to RegistrationActivity
                regID = "unknown";
                Log.e(TAG,"registration id is not set in device");
            }
            
            if (method.equals("GET")) {
                
                DefaultHttpClient httpclient = new DefaultHttpClient();
                final HttpParams httpParams = httpclient.getParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
                HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);
                
                // formulate GET request with parameters
                HttpGet httpRequest = new HttpGet(url);
                httpRequest.setHeader("Accept", "application/json");
                httpRequest.setHeader("Content-type", "application/json");
                String header = "Token "+ token + ",ID "+ regID;
                httpRequest.setHeader("Authorization", header);
                Log.i(TAG,"send get request header: "+ header);
                
                // send request
                long t = System.currentTimeMillis();
                response = (HttpResponse) httpclient.execute(httpRequest);
                Log.i(TAG, "HTTPResponse received in [" + (System.currentTimeMillis()-t) + "ms]");
                entity = response.getEntity();
                Log.i(TAG,"entity right after getting it in GET Requests.java: "+entity);
                
            } else if (method.equals("PUT")) {
                
                DefaultHttpClient httpclient = new DefaultHttpClient();
                final HttpParams httpParams = httpclient.getParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
                HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);
                
                // formulate PUT request with parameters
                HttpPut httpRequest = new HttpPut(url);
                // set put data from param
                httpRequest.setEntity(new StringEntity(sendData.toString()));
                // Set HTTP headers
                httpRequest.setHeader("Accept", "application/json");
                httpRequest.setHeader("Content-type", "application/json");
                String header = "Token "+ token + ",ID "+ regID;
                httpRequest.setHeader("Authorization", header);
                Log.i(TAG,"send put request header: "+ header);            // send request
                long t = System.currentTimeMillis();
                response = (HttpResponse) httpclient.execute(httpRequest);
                Log.i(TAG, "HTTPResponse received in [" + (System.currentTimeMillis()-t) + "ms]");
                entity = response.getEntity();
                Log.i(TAG,"entity right after getting it in Requests.java: "+entity);
            
            } else if (method.equals("POST")) {
                
                DefaultHttpClient httpclient = new DefaultHttpClient();
                final HttpParams httpParams = httpclient.getParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
                HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);
                
                // formulate POST request with parameters
                HttpPost httpRequest = new HttpPost(url);
                // set post data from param
                httpRequest.setEntity(new StringEntity(sendData.toString()));
                // Set HTTP headers
                httpRequest.setHeader("Accept", "application/json");
                httpRequest.setHeader("Content-type", "application/json");
                String header = "Token "+ token + ",ID "+ regID;
                httpRequest.setHeader("Authorization", header);
                Log.i(TAG,"send post request header: "+ header);            // send request
                long t = System.currentTimeMillis();
                response = (HttpResponse) httpclient.execute(httpRequest);
                Log.i(TAG, "HTTPResponse received in [" + (System.currentTimeMillis()-t) + "ms]");
                entity = response.getEntity();
                Log.i(TAG,"entity right after getting it in Requests.java: "+entity);
                
            } else {
                Log.wtf(TAG,"malformed param of asynctask. unknown method provided: "+method);
                return "illegal method";
            }
            
            if (entity != null) {
                // read datastream
                InputStream instream = entity.getContent();
                resultString = convertStreamToString(instream);
                instream.close();
            } else {
                // entity is null
                Log.i(TAG,"entity is null in "+activity);
                return "entity null";
            }
    
        } catch (Exception e) {
            Log.e(TAG,"Connection Error"); 
            // log and print stack trace, perhaps go back? perhaps return some key that describes error to interface as response
            e.printStackTrace();
            //resultString = "{\"status\":\"exception e\"}";
            // error connecting to server: go back to the loginview screen and inform user that notification was not sent.
            return "request/response error";	
        }
        return resultString;
    }
   
   @Override
    protected void onPostExecute(String result) {
        Log.i("rfiDOG","close progdialog");
        progDialog.dismiss();
        Log.i(TAG,"Requests.java request response: "+result+" in activity "+activity);
        if (result.equals("entity null") || result.equals("json error")) {
            // server error and back	
            if (!isCancelled()) {
                alertBox.alertAndBack(activity,"A server error ocurred.","Okay");
            }
        } else if (result.equals("illegal method")) {
            // check activity code for illegal method. put/get/post are allowed.
            // alert and quit
            if (!isCancelled()) {
                alertBox.quitApplicationDialog(activity,"Unrecoverable Error Occurred","Quit");
            }
        }
        
        try {
            JSONObject output = new JSONObject(result);
            // does "token" key exist in webapp response?
            if (output.has("detail")) {
                // login successful
                String detail = output.getString("detail");

                if (detail.equals("Invalid token")) {
                    // login expired
                    
                    //delete token
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
                    Editor editor = prefs.edit();
                    editor.remove("token");
                    editor.commit();
                    // go to loginview with alert that tells user to log in again
                    Intent intent = new Intent(activity,LoginViewActivity.class);
                    intent.putExtra("popup","Session has expired. Login again.");
                    activity.startActivity(intent);
                    activity.finish();
                    
                } else {
                    // detail has different value
                }
            } else {
                // handle other API responses in specified activities 
            }

        } catch (JSONException e) {
           // handle specific jsonerror cases in separate activities
        }
        
        // give the remaining results to individual processFinish methods of the Activities
        delegate.processFinish(result);
    }
    
   public static String convertStreamToString(InputStream is) {
       
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
