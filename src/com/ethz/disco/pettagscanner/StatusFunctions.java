package com.ethz.disco.pettagscanner;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * The StatusFunctions class defines a number of 
 * functions that facilitate us to do frequent tasks without code reuse.
 */
public class StatusFunctions {
	static final String TAG = "rfiDOG";
	
	/**
	 * Gets GCM registrationID from shared preferences.
	 * If no registrationID is found, returns empty string.
	 * If appVersion has changed, it returns empty string, which
	 * causes the application to obtain a new ID from GCM cloud server.
	 */
	public String getRegistrationId(Activity activity) {
		final SharedPreferences prefs = getGCMPreferences(activity);
		String registrationId = prefs.getString("registration_id", "");
		if (registrationId.isEmpty()) {
			Log.i("rfiDOG", "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt("appVersion",
				Integer.MIN_VALUE);
		int currentVersion = getAppVersion(activity);
		if (registeredVersion != currentVersion) {
			Log.i("rfiDOG", "App version changed.");
			return "";
		}
		return registrationId;
	}
	
	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Activity activity) {
		try {
			PackageInfo packageInfo = activity.getPackageManager()
				.getPackageInfo(activity.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}
	
	/**
	 * @return Application's shared preferences.
	 */
	public SharedPreferences getGCMPreferences(Activity activity) {
		// return apps sharedpreferences where we store persistent app data
		return PreferenceManager.getDefaultSharedPreferences(activity);

	}
	
	/**
	 * @return Application's API authentication token from the shared preferences.
	 */
	public String fetchToken(Activity activity) {
		
		final SharedPreferences prefs = getGCMPreferences(activity);
		String tok = prefs.getString("token", "");
		Log.i(TAG,"fetchtoken token: "+tok);
		if (tok.isEmpty()) {
			Log.i("rfiDOG","token (not found): "+ tok);
			return "";
		}
		
		Log.i("rfiDOG","token: "+ tok);
		
		return tok;
    }
	
	/**
	 * class LoggedInStatus checks if a user is still logged in. If not
	 * it redirects to the login view.
	 */
	public class LoggedInStatus extends Object implements AsyncResponse {
		
		private String url;
		private Activity activity;
		public String token;
		
		public LoggedInStatus(Activity act) {
			activity = act;
			// gets token from shared preferences
			token = fetchToken(activity);
			if (token.isEmpty()) {
				// no login found
				Log.i(TAG,"token empty. back to loginview");
				act.startActivity(new Intent(activity,LoginViewActivity.class));
				act.finish();
			} else {
				// validate token in api
				url = activity.getResources().getString(R.string.api_url) + activity.getResources().getString(R.string.validate_uri);
				final Request request = new Request(activity);
				request.delegate = this;
				request.execute(url,"GET",activity);
			}
		}
		
	    /**
	     * Once API request result comes in, this method is called.
	     * The data either contains a sucess or a failure message from the API.
	     * If a success message is returned, the user is still logged in and the method does nothing further.
	     * If a failure message is returned, the method redirects to login screen.
	     */
		public void processFinish(String response) {
			// postexecute result of token validate
			boolean success = false;
			try {
				Log.i(TAG,"loggedinstatus server response: "+response+" in activity "+activity);
				JSONObject output = new JSONObject(response);
				String status = output.getString("status");
				if (status.equals("success")) {
					success = true;
				} else if (status.equals("unauthorized")) {
					// token not valid
					// success false
				} else {
					// unknown response from server
					// success false
				}
			} catch (JSONException e) {
				// json error in server response
				// success false
			}
			if (!success) {
				// alert user of expired session
				// delete token from prefs and go to loginview
				
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
				Editor editor = prefs.edit();
				editor.remove("token");
				editor.commit();
				// go to loginview with alert that tells user to log in again
				Intent intent = new Intent(activity,LoginViewActivity.class);
				intent.putExtra("popup","Session has expired. Login again.");
				activity.startActivity(intent);
				activity.finish();
			}
		}
	}
	
	/**
	 * method works in conjunction with LoggedInStatus class to ascertain if a user is still logged in.
	 */
	public void amILoggedIn(Activity activity) {
		Log.i(TAG,"called amILoggedIn from activity: "+activity);
		LoggedInStatus s = new LoggedInStatus(activity);
		// if empty/invalid redirected to loginviewactivity
		
	}
	
	/**
	 * method logs out user by removing the token from the shared preferences
	 */
	public void logOut(Activity activity) {
		// remove token from sharedprefs
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
		Editor editor = prefs.edit();
		editor.remove("token");
		editor.commit();
		Log.i("rfiDOG","removing token from prefs. " + prefs.getString("token","")+".");
		
		// go to loginview
		Intent intent = new Intent(activity,LoginViewActivity.class);
		activity.startActivity(intent);
		activity.finish();
	}
	
	
	/**
	 * start a phonecall using the phone number parameter.
	 */
	public void callPhone(Activity activity, String phoneNumber) {
		Intent callIntent = new Intent(Intent.ACTION_CALL);
		callIntent.setData(Uri.parse("tel:"+phoneNumber));
		activity.startActivity(callIntent);
		activity.finish();
	}
	
	/**
	 * Stores the registration ID and app versionCode in the application's
	 * {@code SharedPreferences}.
	 *
	 * @param context application's context.
	 * @param regId registration ID
	 */
	public void storeRegistrationId(Activity activity, String regId, String PROPERTY_REG_ID, String PROPERTY_APP_VERSION) {
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
		int appVersion = getAppVersion(activity);
		Log.i(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}
	
	/**
	 * Sends the registration ID to your server over HTTP, so it can use HTTP
	 * to send messages to your app. Not needed for this demo since the
	 * device sends upstream messages to a server that echoes back the message
	 * using the 'from' address in the message.
	 */
	public void sendRegistrationIdToBackend(Activity activity,String regId) {
		// CURRENTLY: sending regID handled in every Requests.java Request object in request authentication header, and only validated on server at login.
	}
}
