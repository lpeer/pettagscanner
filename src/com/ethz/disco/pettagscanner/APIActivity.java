package com.ethz.disco.pettagscanner;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

/**
 * This Activity receives an animal tag and GPS location from previous Activity and
 * sends a lost animal record initialization request to the API.
 */
public class APIActivity extends MainMenuActivity implements AsyncResponse {

    static final String TAG = "rfiDOG";
    private String url;
    private String tag;
    //private String token;
    private String comment;
    private double latitude;
    private double longitude;
    //private GPSTracker GPS;
    //private NewGPSTracker GPS;
    Bundle extras;
    private String anon = "";
    
    //instantiate the HttpRequest AsyncTask
    Request request = new Request(APIActivity.this);
    
    //add status check helper functions and alert functions
    //private StatusFunctions statusFunction = new StatusFunctions();
    private APIAlertDialogs alertBox = new APIAlertDialogs();
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // cancel asynctask if activity is closed
        request.cancel(true);
    }
    
    @Override
    protected void onRestart() {
        super.onRestart();
        //statusFunction.amILoggedIn(this);
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // go to ScanViewActivity on back click
            Intent intent = new Intent(APIActivity.this,ScanViewActivity.class);
            startActivity(intent); 
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        Log.i("rfiDOG", "in APIActivity");
        
        // initialize GPS functionality
        //GPS = new GPSTracker(APIActivity.this,APIActivity.this);
        //GPS = new NewGPSTracker(APIActivity.this);
        // get tag value from intent extras
        if (getIntent().getStringExtra("tag") != null) {
            tag = getIntent().getStringExtra("tag");
            Log.e(TAG,"scanned tag is: "+tag+".");
        } else {
            // tag does not exist in intent extras
            Log.e(TAG,"tag does not exist in extras in APIActivity.");
            // go back to home
            alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class, "An error occurred", "Okay",null);
        }
        if (getIntent().getStringExtra("longitude") != null) {
            try {
            	longitude = Double.parseDouble(getIntent().getStringExtra("longitude"));
            } catch (NumberFormatException e) {
            	Log.e(TAG,"invalid double value for longitude");
        		alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class, "An error occurred getting location", "Okay",null);
            }
        } else {
            // go back to home
            alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class, "An error occurred", "Okay",null);
        }   
        if (getIntent().getStringExtra("latitude") != null) {
        	try {
        		latitude = Double.parseDouble(getIntent().getStringExtra("latitude"));
        	} catch(NumberFormatException e) {
        		Log.e(TAG,"invalid double value for latitude");
        		alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class, "An error occurred getting location", "Okay",null);
        	}
        } else {
            // go back to home
            alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class, "An error occurred", "Okay",null);
        } 
        if (getIntent().getStringExtra("anon") != null) {
        	anon = getIntent().getStringExtra("anon");
        } else {
            // go back to home
        	anon = "false";
        	Log.e(TAG,"no anon set in apiactivity");
            //alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class, "An error occurred", "Okay",null);
        } 
        if (getIntent().getStringExtra("comment") != null) {
        	comment = getIntent().getStringExtra("comment");
        } else {
            // go back to home
        	comment = "";
        	Log.e(TAG,"no comment provided in apiactivity");
            //alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class, "An error occurred", "Okay",null);
        } 
        
        JSONObject sendJSON = new JSONObject();
        try {
            sendJSON.put("longitude", longitude);
            sendJSON.put("latitude", latitude);
            sendJSON.put("tag",tag);
            sendJSON.put("anonymous",anon);
            sendJSON.put("comment", comment);
            
            Log.i(TAG,"sendJSON: "+sendJSON.toString());
            
            // JSON object to POST is generated. send it using async POST request
            url = getResources().getString(R.string.api_url) + getResources().getString(R.string.notification_send_uri);
            // set listener interface on HTTPREQUEST ASYNCTASK
            request.delegate = this;
            request.execute(url,"POST",APIActivity.this,sendJSON);
            
        } catch (JSONException e) {
            // error creating JSON
            e.printStackTrace();
            alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class, "An error occurred", "Okay",null);
        } 
        
    } // done onCreate
    
    /**
     * Once API request result comes in, this method is called.
     * The data contains a success or failure message from the API
     * If successful, the app redirects to the loss notification detail page.
     */
    public void processFinish(String result){
        Log.i(TAG,"in processFinish of APIActivity.");
        // get json data from api response
        try {
            if(result  != null) {
                JSONObject output = new JSONObject(result);
                 
                // does "status" key exist in webapp response json object?
                if (output.has("status")) {
                    String status = output.getString("status");
                    if (status.equals("success")) {
                        // redirect app to notification detail page on success
                        if (output.has("id")) {
                            if (anon.equals("false")) {
                                String id = output.getString("id");
                                Bundle extras = new Bundle();
                                extras.putString("id",id);
                                alertBox.simpleRedirectAlert(APIActivity.this,NotificationActivity.class,"Success! The Animal has been found.","View",extras);
                            } else {
                                alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class,"Success! The Animal has been found and your request is being processed anonymously on the system.","Back",null);
                            }
                        } else {
                            // success but no id?
                            alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class, "An error occurred.", "Okay",null);
                        }
                    } else if (status.equals("you scanned your own animal")) {
                        //The scanner works. you have scanned your own animal
                        alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class, "You have successfully scanned your own animal.", "Okay",null);
                    } else if (status.equals("user does not exist")) {
                        // should not happen
                        Log.wtf(TAG, "Token malfunction? Login was valid but token was not accepted by API.");
                        alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class, "An error occurred", "Okay",null);
                    } else if (status.equals("animal does not exist in database")) {
                        // go to layout that shows error message about record not being in db
                        Log.e(TAG,"status: animal does not exist");
                        alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class, "Unfortunately, the animal does not exist in our database. Please contact the closest shelter, veterinary office or police station.", "Okay",null);
                        // TODO: perhaps new layout view with sad doggy picture?
                    } else {
                        // unknown status message received from api
                        Log.e(TAG,"bad request. response has status key but no known value.");
                        Log.e(TAG,"response: "+result);
                        alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class, "An error occurred", "Okay",null);
                    }
                } else {
                    Log.e(TAG,"no status key in output");
                    alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class, "An error occurred", "Okay",null);
                }
            } else {
                //null result case -> server unreachable?
                Log.e(TAG,"result is null in processFinish");
                alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class, "An error occurred", "Okay",null);
            }
        } catch (JSONException e) {
            // problem extracting JSON data from response
            e.printStackTrace();
            Log.e(TAG,"jsonexception in processFinish. malformed data from api.");
            Log.e(TAG,"response: "+result);
            alertBox.simpleRedirectAlert(APIActivity.this,ScanViewActivity.class, "An error occurred", "Okay",null);
        }
    }    
}

