package com.ethz.disco.pettagscanner;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    private StatusFunctions statusFunction = new StatusFunctions();
    public GcmIntentService() {
        super("GcmIntentService");
    }
    public static final String TAG = "rfiDOG";

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);
        Log.e("rfiDOG","in onhandleintent gcmintentservice");
        Log.e("rfiDOG","MessageType: "+messageType);
        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM will be
             * extended in the future with new message types, just ignore any message types you're
             * not interested in, or that you don't recognize.
             */
        	Log.e("rfiDOG","extras not empty in gcmintentservice");
        	Log.e("rfiDOG",extras.toString());
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                // SEND ERROR REPLY.
                Log.e(TAG,"GCM Send error: " + intent.getExtras().toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                // GCM DELETE MESSAGE NOTIFY
                Log.e(TAG,"GCM Deleted messages on server: " + intent.getExtras().toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // REGULAR PUSH MESSAGE
                // Post notification of received message.
                if (extras.getString("delete") != null) {
                    // remove the regid from device
                    Log.i(TAG, "received in gcmintentservice: " + extras.toString());
                    // Unregister registrationId
                    //startActivity(new Intent(GcmIntentService.this,UnregistrationActivity.class));
                    Log.i(TAG,"deleting registration_id from prefs");
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    Editor editor = prefs.edit();
                    editor.remove("registration_id");
                    editor.commit();
                    
                }
                if (extras.getString("owner") != null) {
                    Log.i(TAG,"owner: "+extras.getString("owner"));
                    Log.i(TAG,"id: "+ extras.getString("id"));
                    String id = extras.getString("id");
                    Log.i(TAG, "received in gcmintentservice: " + extras.toString());
                    // method displays the notification on the phone
                    sendNotification(extras);
                }
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it
    private void sendNotification(Bundle msg) {
    	Log.e("rfiDOG","in sendnotification of gcmintentservice");
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        String description = msg.getString("owner") + "! " + msg.getString("animalname") + " has just been found!";
        mNotificationManager = (NotificationManager)
            this.getSystemService(Context.NOTIFICATION_SERVICE);
        // call "internal" intent from external (receiver) which handles notification inside the app
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,new Intent(this, LandingActivity.class).putExtra("id",msg.getString("id")).putExtra("animalname", msg.getString("animalname")).putExtra("username", msg.getString("owner")), PendingIntent.FLAG_CANCEL_CURRENT);
        //PendingIntent contentIntent = PendingIntent.getActivity(this, 0,new Intent(this, NotificationActivity.class).putExtra("id",msg.getString("id")).putExtra("animalname", msg.getString("animalname")).putExtra("username", msg.getString("owner")), PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
            .setContentTitle("Yay!")
            .setSmallIcon(R.drawable.dog_logo)
            .setStyle(new NotificationCompat.BigTextStyle().bigText(description))
            .setContentText(description)
            .setAutoCancel(true)
            .setOnlyAlertOnce(true)
            .setSound(uri)
            .setDefaults(Notification.DEFAULT_VIBRATE);
        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}