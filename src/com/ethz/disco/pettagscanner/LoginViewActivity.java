package com.ethz.disco.pettagscanner;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;


/**
 * This Activity displays a login view where a user can try to log in using his credentials.
 * If the user clicks the login button, the LoginActivity is executed.
 */
public class LoginViewActivity extends Activity implements AsyncResponse {

	StatusFunctions statusFunction = new StatusFunctions();
	APIAlertDialogs alertBox = new APIAlertDialogs();
	private String referer; 
	private String initialusername;
	SharedPreferences prefs;
	Context context;
	String token;
	static final String TAG = "rfiDOG";
	
	// instantiate HttpRequest AsyncTask
	Request request = new Request(LoginViewActivity.this);
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			alertBox.quitOrNop(LoginViewActivity.this);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	public void processFinish(String response) {
					try {
						Log.i(TAG,"server response: "+response);
						JSONObject output = new JSONObject(response);
						String status = output.getString("status");
						if (status.equals("success")) {
							// token is valid. redirect to home
							Intent intent = new Intent(LoginViewActivity.this,ScanViewActivity.class);
							startActivity(intent);
							overridePendingTransition(0,0);
							finish();
						} else {
							// unknown response from server
						    // not logged in
						}
					} catch (JSONException e) {
						// json error in server response
						// not logged in
					}			
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		Log.e("rfiDOG", "in loginview activity");
		setContentView(R.layout.login);
		context = getApplicationContext();
		
		// display error dialogs
        String error = getIntent().getStringExtra("popup");
        if (error != null) {
        	alertBox.simpleAlert(LoginViewActivity.this,error,"Okay");
        }
		
		// fetch token if it exists and is valid move on to homeactivity
		token = statusFunction.fetchToken(this);
		if (token.isEmpty()) {
			// not logged in. stay in activity
		} else {
			// token exists in shared preferences. is it valid?
			String url = getResources().getString(R.string.api_url) + getResources().getString(R.string.validate_uri);
			request.delegate = this;
			request.execute(url,"GET",this);
			// only redirects to main app if login is valid
		}
		
		// get error messages from intent and set field values
		TextView errorText = (TextView) findViewById(R.id.loginerrorsfield);
		EditText editText_usr = (EditText) findViewById(R.id.loginusername);
		EditText editText_pwd = (EditText) findViewById(R.id.loginpassword);
		Intent intent = getIntent();
		if (intent.getStringExtra("error") != null) {
			errorText.setText(intent.getStringExtra("error"));
		} else {
			errorText.setText("");
		}
		if (intent.getStringExtra("username") != null) {
			editText_usr.setText(intent.getStringExtra("username"));
		} else {
			editText_usr.setText("");
		}
		if (intent.getStringExtra("password") != null) {
			editText_pwd.setText(intent.getStringExtra("password"));
		} else {
			editText_pwd.setText("");
		}
		if (intent.getStringExtra("referer") != null) {
			referer = intent.getStringExtra("referer");
		} else {
			referer = "";
		}
		if (intent.getStringExtra("initialusername") != null) {
			initialusername = intent.getStringExtra("initialusername");
		} else {
			initialusername = "";
		}

		
		// set links to register/forgot password (redirect to webapp)
		TextView registerLink = (TextView) findViewById(R.id.link_to_register);
		registerLink.setMovementMethod(LinkMovementMethod.getInstance());			
		TextView forgotPasswordLink = (TextView) findViewById(R.id.forgotpasswordlink);
		forgotPasswordLink.setMovementMethod(LinkMovementMethod.getInstance());
		// user clicks login
		findViewById(R.id.btnLogin).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						
						// pass username/password/referer extras to loginactivity
						EditText usernameField = (EditText) findViewById(R.id.loginusername);
						String username = usernameField.getText().toString();
						EditText passwordField = (EditText) findViewById(R.id.loginpassword);
						String password = passwordField.getText().toString();
						
						Intent i = new Intent(LoginViewActivity.this,LoginActivity.class);
						i.putExtra("username", username);
						
						i.putExtra("password", password);
						if (!initialusername.equals("")) {
							if (initialusername.equals(username)) {
								// user tries to access his own new lossnotification
								i.putExtra("referer", referer);
								i.putExtra("initialusername",initialusername);
							} else {
								// different user tries to access lossnotification. don't refer.
							}
						} else {
							// not trying to access lossnotification. referer can stay.
							i.putExtra("referer", referer);
						}
						
						
						// findViewById(R.id.btnLogin).playSoundEffect(0);
						startActivity(i);
						overridePendingTransition(0, 0);
						finish();
					}
				});
	}
}
