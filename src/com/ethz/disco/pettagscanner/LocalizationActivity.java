package com.ethz.disco.pettagscanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.location.LocationListener;

/**
 * This Activity receives an animal tag from the previous ScanActivity and
 * calculates the current GPS location of the smartphone. It asks the user
 * about the desired privacy setting and then it gives control to the next 
 * activity, APIActivity, with the calculated location and the received tag 
 * in the intent extras.
 */
public class LocalizationActivity extends Activity implements LocationListener {
	
    static final String TAG = "rfiDOG";
    private String tag;
    private String comment;
    Bundle extras;
    private String anon = "";
    private double latitude;
    private double longitude;
    private APIAlertDialogs alertBox = new APIAlertDialogs();
    private LocationManager locationManager;
    private String provider;
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // go to ScanViewActivity on back click
            Intent intent = new Intent(LocalizationActivity.this,ScanViewActivity.class);
            startActivity(intent); 
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    /* Request updates at resume */
    @Override
    protected void onResume() {
      super.onResume();
      locationManager.requestLocationUpdates(provider, 400, 1, this);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        Log.i("rfiDOG", "in LocalizationActivity");
        
        // set location initialization point
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);
        
        // get location
        if (location != null) {
        	System.out.println("Provider " + provider + " has been selected.");
        	onLocationChanged(location);
        } else {
        	alertBox.simpleRedirectAlert(LocalizationActivity.this,ScanViewActivity.class, "An error occurred", "Okay",null);
        }

    } // done onCreate
    
    
    @Override
    public void onLocationChanged(Location location) {

        // Getting latitude of the current location
        latitude = location.getLatitude();

        // Getting longitude of the current location
        longitude = location.getLongitude();
        
        // move on to next activity after asking about privacy settings
        if (getIntent().getStringExtra("tag") != null) {
            tag = getIntent().getStringExtra("tag");
            Log.e(TAG,"scanned tag is: "+tag+".");
            
            // display alert where user enters a comment text and selects privacy setting between normal and anonymous
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Choose Privacy Mode");
            builder.setCancelable(true);
            LayoutInflater inflater=LocalizationActivity.this.getLayoutInflater();
            View layout=inflater.inflate(R.layout.comments,null);
            builder.setView(layout);
            final EditText commentText = (EditText)layout.findViewById(R.id.commenttext);
            builder.setMessage("Send as Anonymous User?");
            builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                anon = "false";
                comment = commentText.getText().toString();
                sendNotificationActivity(tag,comment);
                dialog.cancel();}});
            builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    anon = "true";
                    comment = commentText.getText().toString();
                    sendNotificationActivity(tag,comment);
                    dialog.cancel();}});
            builder.create();
            builder.show();
            
        } else {
            // tag does not exist in intent extras
            Log.e(TAG,"tag does not exist in extras in LocalizationActivity.");
            // go back to home
            alertBox.simpleRedirectAlert(LocalizationActivity.this,ScanViewActivity.class, "An error occurred", "Okay",null);
        }
        
 
    }
 
	/* Remove the locationlistener updates when Activity is paused */
	@Override
	protected void onPause() {
		super.onPause();
		locationManager.removeUpdates(this);
	}
	
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}
	
	@Override
	public void onProviderEnabled(String provider) {
		Log.i(TAG,"Enabled new provider: "+provider);
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.i(TAG,"Disabled provider: "+provider);
	}
    
    /**
     * Method redirects to APIActivity
     * putting needed values in intent 
     * extras.
     */
    void sendNotificationActivity(String t,String c) {
        Log.i(TAG,"extra comment field on alert to choose privacy value: "+c);
        
        Log.e("rfiDOG","scanned longitude: "+longitude);
        Log.e("rfiDOG","scanned latitude: "+latitude);
        Log.e("rfiDOG","tag: "+t);
        Log.e("rfiDOG","anon: "+anon);
        
        Intent intent = new Intent(LocalizationActivity.this,APIActivity.class);
        intent.putExtra("tag", t);
        intent.putExtra("latitude", String.valueOf(latitude));
        intent.putExtra("longitude", String.valueOf(longitude));
        intent.putExtra("anon",anon);
        intent.putExtra("comment",c);
        startActivity(intent);
        finish();
    }    
}

